# -*- coding: utf-8 -*-

import argparse
import os
import time
import cv2
import numpy as np
np.seterr(all='raise')

from sys import stdout
import screeninfo
import json
from find_peaks import find_peaks
from helpers import _rint


class Detector(object):
    """
    Detector of particles in microscopy videos using techniques of filtering
    to clean images and a local maxima search after apply Laplacian filter.

    Attributes:
        operator: detection algorithm ('log' fro Laplacian-of-Gaussian or 'dog'
                  for Difference-of-Gaussians)
        prescale: A float indicating a scale to resize images before the detection.
        invert: A bool indicate if it have to invert images for a correct local
                maxima search.
        avg: A bool indicating if average sustraction have to be apply.
        sigma: A float indicating the sigma parameter of the gaussian blur.
        thr: A float indicating threshold to consider local maxima correct
             particle detections and local maxima produces by noise.
        subpix: A bool indicating if have to fit a paraboloid to get better coordinates.
        nlmeans: A bool indicating if have to apply non-local means algorithm.
    """

    def __init__(self, operator='log', prescale=1.0, invert=False, avg=True, sigma=1.5, thr=0.6, subpix=True, nlmeans=False):
        self._operator = str(operator)
        self._prescale = float(prescale)
        self._invert = bool(invert)
        self._avg = bool(avg)
        self._avg_img = None
        self._sigma = float(sigma)
        self._thr = float(thr)
        self._subpix = bool(subpix)
        self._nlmeans = bool(nlmeans)

        if not np.allclose(self._prescale, 1.0):
            self._sigma *= self._prescale

    def get_param(self):
        """
        Get parameters of the Detector object in a dictionary
        """
        return {'operator': self._operator,
                'prescale': self._prescale,
                'invert': self._invert,
                'avg': self._avg,
                'sigma': self._sigma,
                'thr': self._thr,
                'subpix': self._subpix,
                'nlmeans': self._nlmeans}

    def set_average(self, srcfile, n=0):
        """
        Set de image avarage of all frames (dtype=float32) if detector have
        the 'avg' option active
        """
        if not n >= 0:
            raise IOError('{} is an invalid number of images'.format(n))

        # load video
        try:
            device = cv2.VideoCapture(srcfile)
        except:
            raise IOError('can\'t open {}'.format(srcfile))

        # calculate index of images to calculate average
        import random
        total_frames = int(device.get(cv2.CAP_PROP_FRAME_COUNT))
        if n == 0 or n > total_frames:
            n = total_frames
        frames_index = random.sample(range(1, total_frames), n - 1)

        # transform images to grayscale and calculate the avarage images
        _, frame = device.read()
        acc = self._image2grayscale(frame)
        # obtain unique index of n frames
        for i in frames_index:
            device.set(cv2.CAP_PROP_POS_FRAMES, i)
            ret, frame = device.read()
            if not ret:
                break
            img = self._image2grayscale(frame)
            acc += img
        self._avg_img = acc / n - 0.5  # shift the mean of the transformed image to 0.5

    def _image2grayscale(self, img):
        """
        Convert an image to grayscale with float32 values between 0-1
        """
        if img.ndim == 3:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        if img.dtype == np.uint8:
            img = img.astype(np.float32) / 255.0

        return img

    @property
    def avg_img(self):
        return self._avg_img

    @avg_img.setter
    def avg_img(self, val):
        self._avg_img = val

    def process(self, srcfile, view=False):
        """
        Transform images to the correct size, grayscale and clear them
        of unwanted static noise and gaussian noise for apply a Laplacian
        filter to get coordinates of particles

        Inputs:
            * srcfile: path to a videofile to preprocess
        """

        # load video
        try:
            device = cv2.VideoCapture(srcfile)
        except:
            raise IOError('can\'t open {}'.format(srcfile))

        # compute the avarage image if it's specificated and no calculated
        if self._avg and self._avg_img is None:
            self.set_average(srcfile)

        img_count = 0
        detections = []
        visu_scale = 0
        # process images one by one
        while True:
            # load frame
            ret, original_img = device.read()
            if not ret:
                break

            # ----- Pre-procesing: Prepare the image to clear it from gaussian noise -----

            img = self._image2grayscale(original_img)

            if self._avg_img is not None:
                img -= self._avg_img

            if self._invert:
                img = 1.0 - img

            # save dimensions for visualization scale
            img_dim = np.shape(img)
            img, scale_factor = self._resize(img)

            # ----- Pre-processing: clear image of gaussian noise -----

            # non-local means denoising
            if self._nlmeans:
                img = self._nlmeans_blur(img)

            # ----- Detection with Laplacian and finding local maxima -----

            if self._operator == 'log':
                resp = self._laplacian_of_gaussian(img)

            elif self._operator == 'dog':
                resp = self._difference_of_gaussians(img)

            else:
                raise RuntimeError('wrong operator')

            local_maxima = self._find_local_maxima(resp) / scale_factor
            detections.append(local_maxima.astype(np.float32))

            img_count += 1

            if view:
                # get the scale of the images for visualization
                if not visu_scale:
                    try:
                        visu_scale = float(screeninfo.get_monitors()[0].height - 128) / \
                                     float(img_dim[0])
                    except:
                        visu_scale = 800. / float(img_dim[0])
                self._view(original_img, local_maxima, visu_scale)

                ch = cv2.waitKey(1)
                if ch & 0xFF == 27 or ch & 0xFF in (ord('q'), ord('Q')):
                    break

        return img_count, detections

    def _resize(self, img):
        """
        Resize an image with the scale of the detector parameter
        """

        # check if self._prescale is element-wise equal within a tolerance to 1.0
        if np.allclose(self._prescale, 1.0):
            return img, 1.0

        # height and width of the scale image (it have to be integer)
        height, width = (_rint(self._prescale * img.shape[0]),
                         _rint(self._prescale * img.shape[1]))

        # scale factor may be a little different than prescale specificated
        scale_factor = np.sqrt(float(height * width) / \
                               float(img.shape[0] * img.shape[1]))

        # resize image
        if height != img.shape[0] or width != img.shape[1]:
            img = cv2.resize(img, (width, height))

        return img, scale_factor

    def _nlmeans_blur(self, img):
        img = cv2.fastNlMeansDenoising((255 * img).astype(np.uint8), h = 0.5 * self._sigma)
        return (img / 255.).astype(np.float32)

    def _laplacian_of_gaussian(self, img):
        img = self._gaussian_blur(img, self._sigma)
        return self._sigma**2 * cv2.Laplacian(img, cv2.CV_32F)

    def _difference_of_gaussians(self, img):
        img1 = self._gaussian_blur(img, self._sigma)
        img2 = self._gaussian_blur(img, self._sigma * 1.2)
        return img2 - img1

    def _gaussian_blur(self, img, sigma):
        """
        Apply gaussian blur filter to an image
        """
        ksize = 2 * _rint(3 * sigma) + 1  # ksize must be odd
        return cv2.GaussianBlur(img, (ksize, ksize), sigma)

    def _find_local_maxima(self, resp):
        """
        Find local maxima in an image and return that coordinates
        """
        # make a copy to avoid modifying the original
        resp = resp.copy()

        # keep only the positive part
        resp[np.where(resp < 1e-4)] = 0.

        # normalize range using the q% percentile
        resp = resp / (np.percentile(resp, q=99.9) + 2**-23)

        peaks = find_peaks(resp, self._thr, False)  # deactivate gravity center subpix

        # get local maxima of paraboloid fit if 'subpix' detector's parameter is active
        if self._subpix:
            xx, yy = np.meshgrid([-1, 0, +1], [-1, 0, +1], indexing='xy')
            for n, (x, y) in enumerate(peaks):
                i, j = int(np.rint(y)), int(np.rint(x))
                resp_3x3 = resp[i-1:i+2, j-1:j+2]
                q = self._fit_paraboloid_2d(xx, yy, resp_3x3)
                # if not np.isfinite(q):
                #     continue
                denom = (4.0 * q[0] * q[1] - q[2]**2)
                delta_x = (q[2] * q[4] - 2.0 * q[1] * q[3]) / (denom + 2**-23)
                delta_y = (q[2] * q[3] - 2.0 * q[0] * q[4]) / (denom + 2**-23)
                peaks[n] += np.array([delta_x, delta_y])

        return peaks

    def _fit_paraboloid_2d(self, x, y, f):
        """
        Adjust a paraboloid to obtain real local maxima (not center of pixels)
        """
        # fit paraboloid: f(x,y) = q0*x^2 + q1*y^2 + q2*x*y + q3*x + q4*y + q5
        x = x.ravel(order='C')
        y = y.ravel(order='C')
        f = f.ravel(order='C')
        Q = np.array([x**2, y**2, x*y, x, y, np.ones(len(x))]).T
        # QtQ = np.sum([np.outer(q, q) for q in Q], axis=0)
        # Qtf = np.sum([f[i] * q for i, q in enumerate(Q)], axis=0)
        QtQ = Q.T.dot(Q)
        Qtf = Q.T.dot(f.reshape(-1, 1))
        return np.linalg.pinv(QtQ).dot(Qtf.reshape(-1, 1)).squeeze()

    def _view(self, img, local_maxima, visu_scale):
        """
        visualization of an image with detections
        """

        # resize image to show it
        visu_img = cv2.resize(img / (img.max() + 2**-23),
                              dsize=(_rint(visu_scale * img.shape[1]),
                                     _rint(visu_scale * img.shape[0])))

        # draw detections in a clear image
        visu_det = np.zeros(visu_img.shape)
        radius = max(1, int(visu_scale * 1.4142 * self._sigma))
        # if 'subpix' option is active, local maxima could be out of the image
        for (x, y) in local_maxima:
            if (0 <= int(visu_scale * x) < visu_det.shape[1]) and \
               (0 <= int(visu_scale * y) < visu_det.shape[0]):
                cv2.circle(visu_det,
                       (int(visu_scale * x), int(visu_scale * y)),
                       radius=radius, color=(0, 0, 1), thickness=2)

        alpha = 0.5

        # merge original image with detection image
        visu = alpha * visu_det + (1 - alpha) * visu_img

        cv2.imshow("Detections", visu)


if __name__ == "__main__":
    # get defaults params of a Detector class
    param = Detector().get_param()

    parser = argparse.ArgumentParser(
        description='Detector',
        add_help=True,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.register('type', 'bool', lambda s: s.lower() in ("yes", "true", "t", "1", "on"))
    parser.add_argument('file', help='input video file', type=str)
    parser.add_argument('--operator', help='detector operator', type=str, default=param['operator'])
    parser.add_argument('--prescale', help='pre-scale factor', type=float, default=param['prescale'])
    parser.add_argument('--invert', help='work on negative images', type='bool', default=param['invert'])
    parser.add_argument('--sigma', help='Laplacian-of-Gaussian smoothing sigma', type=float, default=param['sigma'])
    parser.add_argument('--thr', help='detection threshold', type=float, default=param['thr'])
    parser.add_argument('--subpix', help='compute subpixel coordinates', type='bool', default=param['subpix'])
    parser.add_argument('--avg', help='substract the average image from each frame', type='bool', default=param['avg'])
    parser.add_argument('--n', help='n random frames to calculate average image', type=int, default=0)
    parser.add_argument('--nlmeans', help='apply non-local means denoising', type='bool', default=param['nlmeans'])
    parser.add_argument('--view', help='view detections', default=argparse.SUPPRESS, action='store_true')
    parser.add_argument('--out', help='output file', type=str)
    args = vars(parser.parse_args())

    # check input file
    srcfile = os.path.abspath(args['file'])
    if (not '%' in srcfile) and (not os.path.exists(srcfile)):
        raise IOError('{} doesn\'t exists'.format(srcfile))

    # create a Detector instance with the input parameters
    param = dict([(k, args[k]) for k in param.keys()])
    detector = Detector(**param)
    print('>> detector parameters: {}'.format(detector.get_param()))

    # load the video
    try:
        device = cv2.VideoCapture(srcfile)
    except:
        raise IOError('can\'t open {}'.format(srcfile))

    # output data
    ddict = {}
    ddict['input_file'] = os.path.split(srcfile)[1]
    ddict['timestamp'] = time.ctime()
    ddict['param'] = param
    ddict['frame_count'] = 0
    try:
        ddict['frame_height'] = int(device.get(cv2.CAP_PROP_FRAME_HEIGHT))
        ddict['frame_width'] = int(device.get(cv2.CAP_PROP_FRAME_WIDTH))
        ddict['fps'] = device.get(cv2.CAP_PROP_FPS)
    except:
        ddict['frame_height'] = 512
        ddict['frame_width'] = 512
        ddict['fps'] = 1
    ddict['detections'] = []

    total_frames = int(device.get(cv2.CAP_PROP_FRAME_COUNT))
    print('>> {} frames with size {}x{}'.format(total_frames, ddict['frame_width'], ddict['frame_height']))

    #  calculate avarage image if it is specificated
    if args['avg'] is True:
        avg_srcfile = os.path.splitext(srcfile)[0] + '-avg.npy'
        if os.path.exists(avg_srcfile):
            print('>> loading already computed background model')
            detector.avg_img = np.load(avg_srcfile)
        else:
            print('>> computing background model ...', end=''), stdout.flush()
            detector.set_average(srcfile, args['n'])
            print('done')

            if detector.avg_img is not None:
                np.save(avg_srcfile, detector.avg_img)

                avg_img = (detector.avg_img - detector.avg_img.min()) / \
                          (detector.avg_img.max() - detector.avg_img.min() + 2**-23)
                cv2.imwrite(avg_srcfile.replace('.npy', '.png'),
                            (255 * avg_img).astype(np.uint8))
                print(' >> background model saved to \'{}\''.format(avg_srcfile + '.npy'))

    print('>> processing ...', end=''), stdout.flush()
    frame_count, detections = detector.process(srcfile, view=bool('view' in args))
    ddict['frame_count'] = frame_count
    ddict['detections'] = [det.tolist() for det in detections]

    if ddict['frame_count'] != total_frames:
        raise RuntimeError('wrong frame count. Check videofile and run again')

    print('>> process finished', end=''), stdout.flush()

    # create a dstfile name if it is not specificated
    dstfile = None if 'out' not in args else args['out']
    if dstfile is None:
        default = Detector().get_param()
        param = {k: v for k, v in param.items() if default[k] != v}
        if len(param) > 0:
            param_str = '-'.join([':'.join((k.lstrip('-'), str(v))) for k,v in param.items()])
            dstfile = os.path.splitext(srcfile)[0] + '-' + param_str + '.detections'
        else:
            dstfile = os.path.splitext(srcfile)[0] + '.detections'

    # save 'ddict' in a json file
    with open(dstfile, 'w') as outfile:
        json.dump(ddict, outfile)
    print('results saved to \'{}\''.format(dstfile))
