from detector import *
from nose.tools import assert_equals, assert_not_equal, with_setup
import cv2
import numpy as np
import random
import os

class TestDETECTOR():
    """ Test detector functions

    Create a video with one ball that simulate a particle and test
    the functions of filtering and detections of the class Detector
    """

    @classmethod
    def setup_class(cls):
        # create a Detector object
        cls.param = {'prescale': 1.0,
                     'invert': True,
                     'avg': False,
                     'sigma': 5.0,
                     'thr': 0.5,
                     'subpix': False,
                     'nlmeans': False}
        cls.detector = Detector(**cls.param)

        # Define the codec and create VideoWriter object for save video files and average image
        cls.frame_size = [200, 200]
        fourcc = 0
        folder = 'test_files'
        cls.output_file = '{}/particles.avi'.format(folder)
        cls.avg_img_file = '{}/particles_avg.jpg'.format(folder)
        cls.output_file_detections = '{}/detections.avi'.format(folder)
        if not os.path.exists(folder):
            os.makedirs(folder)

        out_video = cv2.VideoWriter(cls.output_file, fourcc, 30.0, (cls.frame_size[0], cls.frame_size[1]))

        # create a set of images represanting a video with a ball moving
        cls.n_frames = 50
        ball_radio = 7
        max_move = 2
        cls.frames = [np.zeros((cls.frame_size[0], cls.frame_size[1], 3)).astype(np.uint16) for i in range(cls.n_frames)]
        cls.balls = []
        random.seed(9009)
        for i, frame in enumerate(cls.frames):
            if i == 0:
                # create an initial random place for the ball
                x_ball = random.randint(0, cls.frame_size[1] - 1)
                y_ball = random.randint(0, cls.frame_size[0] - 1)
            else:
                # simulate a move in a square around the last frame
                x_move = random.randint(-max_move, max_move)
                y_move = random.randint(-max_move, max_move)
                # if one of the movement axis falls out of the image turn back in that axis
                x_move = x_move if (0 <= x_ball + x_move < cls.frame_size[1]) else -x_move
                y_move = y_move if (0 <= y_ball + y_move < cls.frame_size[0]) else -y_move
                x_ball += x_move
                y_ball += y_move
            cls.balls.append([x_ball, y_ball])
            # draw the ball in the frame
            cv2.circle(frame, (x_ball,y_ball), ball_radio, (122,122,122), -1)

            # write image in the video file
            out_video.write(np.uint8(frame))
        out_video.release()

    def test_detector_create(self):
        # test if it could instance a Detector object
        assert_not_equal(self.detector, None)

    def test__image2grayscale(self):
        # check if dimensions after apply are ok (image of only one channel)
            dim = np.shape(self.detector._image2grayscale(self.frames[0]))
            assert_equals(len(dim), 2)

    def test__resize(self):
        # check if dimensions after apply are ok
        resize_img, scale = self.detector._resize(self.frames[0])
        assert_equals(np.shape(resize_img), (self.frame_size[0] * scale,
                                             self.frame_size[1] * scale,
                                             3))
        assert_equals(scale, 1.0)

    def test_find_local_maxima(self):
        # create local maxima in the coordinates of the particle and try to find it
        frame = self.frames[0]
        ball = self.balls[0]
        frame[ball[0], ball[1]] += 20
        if self.detector._invert:
            frame = 1 - self.detector._image2grayscale(frame)
        peak = self.detector._find_local_maxima(frame)
        np.testing.assert_equal(peak, np.array([list(reversed(ball))]))

    def test_get_param(self):
        # test if get_param function return parameters ok.
        assert_equals(set(self.detector.get_param()), set(self.param))

    def test_set_average(self):
        # compute average image with detector function
        self.detector.set_average(self.output_file, 0)

        # load images and calculate average image. Not use self.frames because
        # images in ouput_file had loss quality when write with cv2.VideoWriter
        device = cv2.VideoCapture(self.output_file)
        _, frame = device.read()
        gray_images = self.detector._image2grayscale(frame)
        for i in range(1, self.n_frames):
            _, frame = device.read()
            gray_images += self.detector._image2grayscale(frame)
        avg_img = gray_images / self.n_frames - 0.5

        np.testing.assert_equal(self.detector.avg_img, avg_img)
        cv2.imwrite(self.avg_img_file, np.uint8(avg_img * 255))

    def test_process(self):
        # process with a video where the ball always stay completly in scene
        # (not in the borders)
        img_count, detections = self.detector.process(self.output_file, view=False)

        # check detections
        for i, det in enumerate(detections):
            # if no detections by the Detector, continue to compare others detections
            # DELETE: cuando esta cerca del borde las detecciones pueden no ser exactas, entonces habría que comparar de otra forma.
            if len(det) <= 0:
                continue

            assert_equals(len(det), len(np.array([self.balls[i]]))) # compare length
            np.testing.assert_equal(det, np.array([self.balls[i]])) # compare detections

        # save video with detections
        fourcc = 0
        out_video = cv2.VideoWriter(self.output_file_detections, fourcc, 30.0, (self.frame_size[0], self.frame_size[1]))
        visu_scale = self.detector._prescale
        for i, frame in enumerate(self.frames):
            img = frame # quizas 2gray
            visu_img = cv2.resize(img / img.max(),
                                  dsize=(int(np.rint((visu_scale * img.shape[1]))),
                                         int(np.rint((visu_scale * img.shape[0])))))
            visu_det = np.zeros(np.shape(frame))
            radius = max(1, int(visu_scale * 1.4142 * self.detector._sigma))
            # print(det)
            cv2.circle(visu_det,
                       (int(visu_scale * detections[i][0][0]), int(visu_scale * detections[i][0][1])),
                       radius=radius, color=(0, 0, 1), thickness=2)
            alpha = 0.5

            # merge original image with detection image
            visu = alpha * visu_det + (1 - alpha) * visu_img
            visu = visu * 255

            out_video.write(np.uint8(visu))
        out_video.release()
