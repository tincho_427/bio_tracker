# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import numpy as np
import rdp
from helpers import degree

class CHDDetector(object):
    """
    Change directions detector for tracks
    """

    def __init__(self, min_degree=0, max_degree=180, epsilon=10.0, min_dist=10):
        self._min_degree = int(min_degree)
        self._max_degree = int(max_degree)
        self._epsilon = float(epsilon)
        self._min_dist = float(min_dist)

    def get_param(self):
        """
        Get parameters of the TumblesDetector object in a dictionary
        """
        return {'min_degree': self._min_degree,
                'max_degree': self._max_degree,
                'epsilon': self._epsilon,
                'min_dst': self._min_dist}

    def get_tumbles(self, tracks):
        """
        Return a list where each element is a tumble dictionary (corresponding
        to each track) with keys: 'pts' and 'degrees'
        """
        tumbles = []
        degrees = []
        for tr in tracks:
            points = tr['pt']
            x, y = np.array(points).T

            # ----- STEP 1: apply rdp algorithm -----
            simplified = np.array(rdp.rdp(points, self._epsilon))
            sx, sy = simplified.T

            # calculate degrees for filter
            directions = np.diff(simplified, axis=0)
            theta = degree(directions) * 180 / np.pi

            # filter tumbles between min_degree and max_degree
            idx = np.where((self._min_degree <= theta) & (theta <= self._max_degree))[0]

            # ----- STEP 2: filter points that move at least min n_bodies -----
            if len(idx) > 0:
                points = np.concatenate((np.reshape(points[0], (1,2)), simplified[idx+1], np.reshape(points[-1], (1,2))), axis=0)
                # Forward pass
                points_filtered = [points[0]]
                for i in range(1, len(points) - 1):
                    prev_pt = points_filtered[-1]
                    dist = np.sqrt((prev_pt[0] - points[i][0])**2 + (prev_pt[1] - points[i][1])**2)
                    if dist >= self._min_dist:
                        points_filtered.append(points[i])
                # Backward pass
                # add the last point again
                points = np.concatenate((np.array(points_filtered), np.reshape(points[-1], (1,2))), axis=0)
                points_filtered = [points[-1]]
                for i in range(1, len(points) - 1):
                    prev_pt = points_filtered[-1]
                    dist = np.sqrt((prev_pt[0] - points[-1-i][0])**2 + (prev_pt[1] - points[-1-i][1])**2)
                    if dist >= self._min_dist:
                        points_filtered.append(points[-1-i])
                # clear new points (add the point[0] again and re ordenate)
                points = np.concatenate((np.array(points_filtered), np.reshape(points[0], (1,2))), axis=0)[::-1]

                # ----- STEP 3: apply rdp algorithm -----
                simplified = np.array(rdp.rdp(points.tolist(), self._epsilon))
                sx, sy = simplified.T
                # calculate degrees
                directions = np.diff(simplified, axis=0)
                theta = degree(directions) * 180 / np.pi
                # filter tumbles between min_degree and max_degree
                idx = np.where((self._min_degree <= theta) & (theta <= self._max_degree))[0]

            # filter only tumble points in the range (min_degree, max_degree)
            sx = sx[idx + 1]
            sy = sy[idx + 1]
            theta = theta[idx]
            tumbles.append(np.array([sx, sy]).T)
            degrees.append(theta)
        return tumbles, degrees
