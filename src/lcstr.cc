#include "lcstr.h"

#include <cmath>
#include <ostream>

// adaptation from: https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring#Python

int
lcstr(float *s1, int n1, float *s2, int n2, float eps, int &i1, int &i2) {
  int *m = new int[(1 + n1) * (1 + n2)];
  std::fill(m, m + (1 + n1) * (1 + n2), 0);

#define M(i, j) (m[(i) * (1 + n2) + (j)])

  int len = 0;
  for (int i = 1; i < 1 + n1; ++i) {
    int len_i = 0;
#pragma omp parallel for reduction(max: len_i)
    for (int j = 1; j < 1 + n2; ++j) {
      if (std::fabs(s1[i - 1] - s2[j - 1]) < eps) {
        M(i, j) = M(i - 1, j - 1) + 1;
        // if (M(i, j) > len) {
        //   len = M(i, j);
        //   i1 = i;
        //   i2 = j;
        // }
        len_i = std::max(len_i, M(i, j));
      } else {
        M(i, j) = 0;
      }
    }

    if (len_i > len) {
      // if len changed to a greater value, look where this happened
      int *mi = m + i * (1 + n2);
      for (int j = 1; j < 1 + n2; ++j) {
        if (len_i > mi[j])
          continue;
        i1 = i;
        i2 = j;
        len = len_i;
        break;
      }
    }
  }

  delete[] m;
  i1 -= len;
  i2 -= len;
  return len;
}
