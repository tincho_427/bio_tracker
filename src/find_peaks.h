#ifndef SRC_FIND_PEAKS_
#define SRC_FIND_PEAKS_

// class PeaksFinder {
//  public:
//   PeaksFinder(float threshold, bool subpixel);
//   ~PeaksFinder();
//   int process(float *img, int width, int height);
//   void peaks(float *output);
//  private:
//   float threshold_;
//   bool subpixel_;
//   int count_;
//   int bsize_;
//   float *buffer_;
// };

extern "C" {
  // PeaksFinder* PeaksFinder_new(float threshold, bool subpixel);
  // void PeaksFinder_del(PeaksFinder* obj);
  // int PeaksFinder_process(PeaksFinder *obj, float *img, int width, int height);
  // void PeaksFinder_peaks(PeaksFinder *obj, float *out);

  int find_peaks(float *out, float *img, int width, int height, float threshold, bool subpixel);
}
#endif  /* SRC_FIND_PEAKS_ */
