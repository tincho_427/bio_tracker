#ifndef SRC_LCSTR_
#define SRC_LCSTR_

extern "C" {
  int lcstr(float *s1, int n1, float *s2, int n2, float eps, int &i1, int &i2);
}
#endif  /* SRC_LCSTR_ */
