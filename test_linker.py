from linker import *
from nose.tools import assert_equals, assert_not_equal, with_setup
import cv2
import numpy as np
# import random

class TestLINKER():
    """ Test linker functions

    Create two trajectories with different length and test the functions of
    link the simulated particles along the time
    """

    @classmethod
    def setup_class(cls):
        # create two Linker objects. One with Kalman option set in True and other False.
        cls.param = {'kalman': False,
                      'min_dist': 8.0,
                      'mean_dist_thr': 2.0,
                      'dist_ratio_thr': 0.5,
                      'max_tgap': 2,
                      'n_dry_run': 0,
                      'kf_long': 0,
                      'kf_radio': 0}
        cls.param_k = {'kalman': True,
                      'min_dist': 12.0,
                      'mean_dist_thr': 10,
                      'dist_ratio_thr': 8,
                      'max_tgap': 3,
                      'n_dry_run': 5,
                      'kf_long': 10,
                      'kf_radio': 8}

        cls.linker = Linker(**cls.param)
        cls.linker_k = Linker(**cls.param_k)

        # Define image dimension that limit the movement of the particles and
        # define folders
        cls.frame_size = [100, 100]
        folder = 'test_files'
        if not os.path.exists(folder):
            os.makedirs(folder)
        cls.output_file = '{}/link_NOmiss.jpg'.format(folder)
        cls.output_file = '{}/link_miss.jpg'.format(folder)
        cls.output_file = '{}/link_NOmiss_kalman.jpg'.format(folder)
        cls.output_file = '{}/link_miss_kalman.jpg'.format(folder)

        # create a set of detections represanting a detections in each frame
        cls.n_frames = 30

        # get detections of a particle that move like sin
        freq_sin = 2
        x_sin = np.linspace(0, 2 * np.pi, cls.n_frames)
        y_sin = [np.sin(i) * 30 + 60 for i in x_sin]
        x_sin = np.linspace(10, 90, cls.n_frames)
        cls.pts_sin = np.vstack(np.broadcast(x_sin, y_sin))

        # get detections of a particle that move like tan
        x_tan = np.linspace(-np.pi, np.pi, cls.n_frames)
        y_tan = [np.tan(2*np.pi * freq_sin * (i / cls.n_frames)) * 30 + 60 for i in x_tan]
        x_tan = np.linspace(30, 70, cls.n_frames)
        cls.pts_tan = np.vstack(np.broadcast(x_tan, y_tan))

        # # to show: create an empty image and draw the detections
        # img = np.zeros((cls.frame_size[0], cls.frame_size[1], 3), np.uint8)
        # for i in range(cls.n_frames):
        #     cv2.circle(img, tuple(np.int_(cls.pts_sin[i])), radius=1, color=(0, 0, 255), thickness=1)
        #     cv2.circle(img, tuple(np.int_(cls.pts_tan[i])), radius=1, color=(0, 255, 0), thickness=1)
        # cv2.imwrite('tracks.jpg', img)

        # simulate points en each frame. One simulation without missing observations, and one with missing observations
        cls.pts = []
        cls.pts_miss = []
        cls.idx_miss_sin = [3, 5, 7, 8]
        cls.idx_miss_tan = [4, 5, 9, 21, 29]
        for i in range(cls.n_frames):
            # cls.pts.append([cls.pts_sin[i], cls.pts_tan[i]])
            cls.pts.append([cls.pts_sin[i]])
            pts_not_miss = []
            if i not in cls.idx_miss_sin:
                pts_not_miss.append(cls.pts_sin[i])
            # if i not in cls.idx_miss_tan:
            #     pts_not_miss.append(cls.pts_tan[i])
            cls.pts_miss.append(pts_not_miss)

    def test_linker_create(self):
        # test if it could instance a Linker object with Kalman and without it
        assert_not_equal(self.linker, None)
        assert_not_equal(self.linker_k, None)

    def test__get_covariance_matrix(self):
        # check if the shape of the covariance matrix it's ok
        cov_mat = self.linker_k._get_covariance_matrix(self.pts_sin, int(len(self.pts_sin) / 3))
        assert_equals(np.shape(cov_mat), (4, 4))

    def test_new_track_kalman(self):
        # test if new track method for kalman criteria its ok
        t = 5
        pt = [32, 56]
        cov_mat = np.eye(4) * 1000
        track = self.linker_k.new_track_kalman(t, pt, cov_mat)
        correct_track_keys = ['t', 'pt', 'inactive', 'kf', 'state_mean', 'cov_mat', 'idx_inactives']
        assert_equals(all([key in correct_track_keys for key in track.keys()]), True)
        assert_equals(track['t'], [t])
        np.testing.assert_equal(track['pt'], [pt])
        np.testing.assert_equal(track['cov_mat'], cov_mat)
        np.testing.assert_equal(track['state_mean'], [pt[0], pt[1], 1, 1])
        np.testing.assert_equal(track['idx_inactives'], [])
        assert_not_equal(track['kf'], None)

    def test_update_track_kalman(self):
        # test if update track method for kalman criteria its ok
        track = self.linker_k.new_track_kalman(5, [32, 56], np.eye(4))
        t = 6
        pt = [33, 52]
        track = self.linker_k.update_track_kalman(track, t, pt)
        assert_equals(track['t'][-1], t)
        np.testing.assert_equal(track['pt'][-1], pt)

    def test_get_idx_asl_tracks(self):
        # check if idx return by the get_idx_asl_tracks are ok
        # empty point to construct tracks with empty observations
        empty_obs = ma.reshape(ma.array((0,0)),(-1,2))
        empty_obs[:] = ma.masked

        # contruct tracks
        tr_as = {'t': [1,2,3], 'pt': self.pts_sin[:3], 'inactive': 1, 'idx_inactives': []} # active short
        tr_al = {'t': [1,2,3,4,5,6,7], 'pt': self.pts_sin[:7], 'inactive': 0, 'idx_inactives': [1]} # active long
        tr_al['pt'][1] = empty_obs
        tr_nas = {'t': [1,2,3,4,5,6], 'pt': self.pts_sin[:6], 'inactive': 3, 'idx_inactives': [3,4,5]} # non active short
        for i in tr_nas['idx_inactives']:
            tr_nas['pt'][i] = empty_obs
        tr_nal = {'t': [1,2,3,4,5,6,7,8,9], 'pt': self.pts_sin[:9], 'inactive': 3, 'idx_inactives': [2,6,7,8]} # non active long
        for i in tr_nal['idx_inactives']:
            tr_nal['pt'][i] = empty_obs
        tracks = [tr_as, tr_al, tr_nas, tr_nal]

        as_tracks, al_tracks = self.linker_k.get_idx_asl_tracks(tracks, 2, 4)
        assert_equals(len(as_tracks), 1)
        assert_equals(as_tracks[0], 0)
        assert_equals(len(al_tracks), 1)
        assert_equals(al_tracks[0], 1)

    def test_tr_pts_mean_distance(self):
        pts = [[1,1], [1,2], [1,5]]
        dist = self.linker_k.tr_pts_mean_distance(pts)
        assert_equals(dist, 2)

    def test__kalman_link_points(self):
        # test without missing observations
        tracks = self.linker_k._kalman_link_points(self.pts, self.n_frames)
        np.testing.assert_equal(tracks[0]['pt'], self.pts_sin)

        # test with missing observations
        # put (0,0) like coordinate of missing observations and compare only true detections
        pts_sin_miss = [self.pts_sin[i] if i not in self.idx_miss_sin else [0,0] for i in range(self.n_frames)]
        # calculate tracks with kalman method
        tracks = self.linker_k._kalman_link_points(self.pts, self.n_frames)
        # put (0, 0) in predicted points
        tracks[0]['pt'] = [tracks[0]['pt'][i] if i not in self.idx_miss_sin else [0,0] for i in range(self.n_frames)]
        # compare
        np.testing.assert_equal(tracks[0]['pt'], pts_sin_miss)

    def test_new_track(self):
        # test if new track method for kalman criteria its ok
        t = 5
        pt = [32, 56]
        track = self.linker.new_track(t, pt)
        correct_track_keys = ['t', 'pt', 'idx_inactives']
        assert_equals(all([key in correct_track_keys for key in track.keys()]), True)
        assert_equals(track['t'], [t])
        np.testing.assert_equal(track['pt'], [pt])
        np.testing.assert_equal(track['idx_inactives'], [])

    def test__link_points(self):
        # test without missing observations
        tracks = self.linker._link_points(self.pts, self.n_frames)
        np.testing.assert_equal(tracks[0]['pt'], self.pts_sin)

        # test with missing observations
        # put (0,0) like coordinate of missing observations and compare only true detections
        pts_sin_miss = [self.pts_sin[i] if i not in self.idx_miss_sin else [0,0] for i in range(self.n_frames)]
        # calculate tracks with kalman method
        tracks = self.linker._link_points(self.pts, self.n_frames)
        # put (0, 0) in predicted points
        tracks[0]['pt'] = [tracks[0]['pt'][i] if i not in self.idx_miss_sin else [0,0] for i in range(self.n_frames)]
        # compare
        np.testing.assert_equal(tracks[0]['pt'], pts_sin_miss)
