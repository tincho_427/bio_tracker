# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import numpy as np
from numpy.ctypeslib import ndpointer
from ctypes import cdll, c_void_p, c_bool, c_float, c_int
from os.path import abspath, join, pardir, dirname, realpath

libutils = cdll.LoadLibrary(abspath(join(dirname(realpath(__file__)), 'build/libutils.so')))

# _peaks_finder_new = libutils.PeaksFinder_new
# _peaks_finder_new.argtypes = [
#     c_float,                # threshold
#     c_bool                  # subpixel (center of mass method)
# ]
# _peaks_finder_new.restype = c_void_p

# _peaks_finder_del = libutils.PeaksFinder_del
# _peaks_finder_del.argtypes = [
#     c_void_p                # obj
# ]
# _peaks_finder_del.restype = c_void_p

# _peaks_finder_process = libutils.PeaksFinder_process
# _peaks_finder_process.argtypes = [
#     c_void_p,                                             # obj
#     ndpointer(dtype=np.float32, ndim=2, flags='C,A'),     # img
#     c_int,                                                # width
#     c_int                                                 # height
# ]
# _peaks_finder_process.restype = c_int

# _peaks_finder_peaks = libutils.PeaksFinder_peaks
# _peaks_finder_peaks.argtypes = [
#     c_void_p,                                             # obj
#     ndpointer(dtype=np.float32, ndim=2, flags='C,A,W'),   # out
# ]
# _peaks_finder_peaks.restype = c_int

_find_peaks = libutils.find_peaks
_find_peaks.argtypes = [
    ndpointer(dtype=np.float32, ndim=2, flags='C,A,W'),   # out
    ndpointer(dtype=np.float32, ndim=2, flags='C,A'),     # img
    c_int,                                                # width
    c_int,                                                # height
    c_float,                                              # threshold
    c_bool,                                               # subpixel
]
_find_peaks.restype = c_int

def find_peaks(img, threshold, subpixel=False):
    assert img.ndim == 2
    h, w = img.shape
    size = (h//2+1) * (w//2+1)
    img = np.require(img, dtype=np.float32, requirements=('C', 'A'))
    out = np.require(np.empty((size, 2)), dtype=np.float32, requirements=('C', 'A', 'W'))
    count = _find_peaks(out, img, int(w), int(h), np.float32(threshold), bool(subpixel))
    return out[:count]
