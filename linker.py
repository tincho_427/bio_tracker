# -*- coding: utf-8 -*-

import argparse
import os
import numpy as np
from numpy import ma
import time
import json
from pykalman import KalmanFilter
from scipy.spatial import distance
from graphs import find_roots, find_leaves, all_paths


def view(tdict, view_points):
    """
    Draw tracks and optionally, each point of each track

    Inputs:
        * tdict: track dictionary (with some necesary fields)
        * view_points: bool type that say if have to draw points (blue like
                       real detections and red like predictions)
    """
    import matplotlib.pyplot as plt
    from matplotlib.cm import jet_r as cmap

    tracks = tdict['tracks']
    plt.figure()
    colors = cmap(np.linspace(0., 1., len(tracks)))
    np.random.seed(111)
    np.random.shuffle(colors)
    for i, tr in enumerate(tracks):
        x, y = np.array(tr['pt']).T
        y_offset = 0.5 * np.random.random()
        if view_points:
            # get points in the track that were predicted with kalman filter
            idx_pred = np.array(tr['idx_inactives'])

            # plot detections points
            x_med = np.array([x for i, x in enumerate(x) if i not in idx_pred])
            y_med = np.array([y for i, y in enumerate(y) if i not in idx_pred])
            plt.plot(x_med, y_med + y_offset, 'x', color='b', ms=8)

            # plot predictions points (with kalman)
            x_pred = np.array([x for i, x in enumerate(x) if i in idx_pred])
            y_pred = np.array([y for i, y in enumerate(y) if i in idx_pred])
            plt.plot(x_pred, y_pred + y_offset, 'x', color='r', ms=8)

        # plot track lines and mark last point of each track
        plt.plot(x, y + y_offset, '-', color=colors[i])
        plt.plot(x[-1], y[-1] + y_offset, 'o', color='w', ms=4)

    plt.axis([0., float(tdict['frame_width'] - 1),
              0., float(tdict['frame_height'] - 1)],
             'equal')
    plt.gca().invert_yaxis()
    plt.grid()
    plt.title('tracks')
    ax = plt.gca()
    ax.set_facecolor('black')
    plt.xlabel('x-coordinate (pixels)')
    plt.ylabel('y-coordinate (pixels)')
    plt.savefig("figure.svg", format="svg")
    plt.show()


class Linker(object):
    """
    Linker of detections used after detect particles with the detection module.
    For this task it could link detections with two methods. One based on a
    'distance between detections' criteria, and other based on Kalman filter
    predictions.

    Attributes:
        kalman: a bool indicating if apply Kalman filter method for link detections.
        min_dist: a float indicating min distance that a particle could
                  jump without consider previous behaviour.
        mean_dist_thr: a float indicating aceleration factor that a particle
                       could have regarding the mean jumping distance to the
                       observed moment.
        dist_ratio_thr: a float indicating threshold between the first and
                        second closest point to the observed detection.
        max_tgap: an integer indicating number of max consecutive moments where it
                  could that allow missing observations and track continue active.
        n_dry_run: an integer indicating the number of frames where the linker algorithm
                   run previously to the real run for get a better initial
                   covariance matrix (related to Kalman filter).
        kf_long: an integer indicating min real detections that a track needs
                 to have for starting to use Kalman filter predictions for
                 take decisions of links.
        kf_radio: a float indicating search radious around the predictions for
                decide if a detection correspond to a track or not.
    """

    def __init__(self, kalman=False, min_dist=5.0, mean_dist_thr=2.0,
                 dist_ratio_thr=0.5, max_tgap=3, n_dry_run= 50, kf_long=8,
                 kf_radio=5):
        self._kalman = bool(kalman)
        self._min_dist = float(min_dist)
        self._mean_dist_thr = float(mean_dist_thr)
        self._dist_ratio_thr = float(dist_ratio_thr)
        self._max_tgap = int(max_tgap)
        self._n_dry_run = int(n_dry_run)
        self._kf_long = int(kf_long)
        self._kf_radio = float(kf_radio)

    def get_param(self):
        """
        Get parameters of the Detector object in a dictionary
        """
        return {'kalman': self._kalman,
                'min_dist': self._min_dist,
                'mean_dist_thr': self._mean_dist_thr,
                'dist_ratio_thr': self._dist_ratio_thr,
                'max_tgap': self._max_tgap,
                'n_dry_run': self._n_dry_run,
                'kf_long': self._kf_long,
                'kf_radio': self._kf_radio}

    def process(self, detections, n_frames):
        """
        Link detections corresponding to the first n_frames using one of the two
        methods: 'distance to other detections' criteria or 'Kalman filter' criteria
        """
        # select method to use
        if self._kalman:
            # run link_points for n_dry_run frames to set a better covariance matrix
            if self._n_dry_run > n_frames:
                self._n_dry_run = n_frames
            if self._n_dry_run > 0:
                init_cov_mat = self._get_covariance_matrix(detections, self._n_dry_run)
                print('>> dry run finished with {} frames'.format(self._n_dry_run))

                # run link_points to get the tracks with kalman
                tracks = self._kalman_link_points(detections, n_frames, init_cov_mat)
            else:
                tracks = self._kalman_link_points(detections, n_frames)
        else:
            tracks = self._link_points(detections, n_frames)

            # connect tracks with diferent tgaps of distance
            max_tgap = self._max_tgap  # save original value
            for tgap in range(1, max_tgap+1):  # eval all tgaps incrementally
                self._max_tgap = tgap
                print('>> tgap = {} ({} tracks)'.format(tgap, len(tracks)), end=' ')
                while True:
                    n = len(tracks)
                    tracks = self._filter_one_pass(tracks)
                    if n == len(tracks):
                        break
                    print('.', end='')
                print('')
            self._max_tgap = max_tgap  # restore value

        return tracks

    def _get_covariance_matrix(self, detections, n_frames):
        """
        Calculate the mean covariance matrix of all tracks
        """
        tracks = self._kalman_link_points(detections, n_frames)
        all_covs = [tr['cov_mat'] for tr in tracks]
        cov_mean = np.average(all_covs, axis=0)
        alpha = 10**-6
        cov_mat = ((cov_mean + cov_mean.T) / 2) + alpha * np.eye(4)
        return cov_mat

    def _kalman_link_points(self, detections, n_frames, init_cov_mat=None):
        """
        Link detections using Kalman filter predictions. To collect enough data
        to get good predictions, first of all, link detections with a 'distance
        to other detections' criteria
        """
        if init_cov_mat is None:
            init_cov_mat = np.eye(4) * 1000

        # an empty observation is used to update the kalman filter where the
        # prediction not match with a detector point
        empty_obs = ma.reshape(ma.array((0,0)),(-1,2))
        empty_obs[:] = ma.masked

        tracks = []
        for t in range(n_frames):
            # coordinates of the points in frame t
            pts_curr = np.atleast_2d(detections[t])
            # This is because if there are no detections, pts_curr is a list
            # with an empty array (so length is equal to 1)
            n_curr = len(pts_curr) if len(pts_curr[0]) else 0

            # no tracks => every point in the current frame sets a new track
            if not tracks and n_curr > 0:
                tracks += [self.new_track_kalman(t, pt, init_cov_mat) for pt in pts_curr]
                continue

            # get indexes of active short and active long tracks
            idx_as_tracks, idx_al_tracks = self.get_idx_asl_tracks(tracks, self._max_tgap, self._kf_long)

            # ----- manage active long tracks -----

            n_al_tracks = len(idx_al_tracks)
            # idx_al_tracked is use to mark long active tracks who join someone in this step
            idx_al_tracked = np.zeros(n_al_tracks, dtype=np.int32)
            for i, idx_track in enumerate(idx_al_tracks):
                # if there are current points try to joins them
                if n_curr > 0:
                    # get the state prediction of the kalman filter
                    state_pred, cov_pred = tracks[idx_track]['kf'].filter(tracks[idx_track]['pt'])
                    state_pred = np.reshape(state_pred[-1], (1,4))

                    # get the min distance of the prediction and the closest
                    # point in the current detections points
                    dist_tr_curr = distance.cdist(state_pred[:,:2], pts_curr, metric='euclidean')
                    min_dist_idx = np.argmin(dist_tr_curr, axis=1)[0]

                    # track found posible join, ie. distance to closest point lower than
                    # kf_radio => join it. Otherwise mark one empty step in the track
                    if dist_tr_curr[0][min_dist_idx] < self._kf_radio:
                        tracks[idx_track] = self.update_track_kalman(tracks[idx_track], t, pts_curr[min_dist_idx])
                        # mark this active long track because it joined a detector point
                        idx_al_tracked[i] = 1
                    else:
                        tracks[idx_track] = self.update_track_kalman(tracks[idx_track], t, empty_obs)
                else:
                    # update active long tracks with empty observations
                    tracks[idx_track] = self.update_track_kalman(tracks[idx_track], t, empty_obs)

            # quit from current points the detections that not match with any
            # active long track
            if sum(idx_al_tracked):
                pts_curr_tracked = np.reshape([tracks[idx_al_tracks[i]]['pt'][-1] for i in np.where(idx_al_tracked)[0]], (-1,2))
                pts_curr = np.reshape([pt for pt in pts_curr if pt not in pts_curr_tracked], (-1,2))
                n_curr = len(pts_curr)

            # ----- manage active short tracks -----

            n_as_tracks = len(idx_as_tracks)
            if n_as_tracks:
                if not n_curr:
                    # If there are no current points, the short active tracks
                    # have to update with an empty observation
                    for idx_track in idx_as_tracks:
                        tracks[idx_track] = self.update_track_kalman(tracks[idx_track], t, empty_obs)
                else:
                    # try to join active short tracks with the 'distance to
                    # other detections' criteria

                    # get last active points from active short tracks
                    pts_act = np.array([tracks[i]['pt'][-1-tracks[i]['inactive']] for i in idx_as_tracks])

                    # distance between current points and active points
                    dist_ca = distance.cdist(pts_curr, pts_act, metric='euclidean')

                    # Separate cases when there are lot of active points and
                    # when there are only one
                    if n_as_tracks > 1:
                        # for each current pt, indices to the closest
                        # and second closest point in the active list
                        idxs_ca_1, idxs_ca_2 = np.argpartition(dist_ca, 1)[:, :2].T
                    else:
                        # if there are only one active pt, supose that the distance
                        # to the second closest point to the actives is infinite
                        idxs_ca_1 = np.argpartition(dist_ca, 0)[:, :1].T[0]
                        idxs_ca_2 = np.zeros((n_curr,), dtype=np.int32) + 1
                        dist_ca2 = np.zeros((n_curr, 1)) + np.inf # Simulate distance to second plosest point with an infinite
                        dist_ca = np.concatenate((dist_ca, dist_ca2), axis=1)

                    # distance between current points
                    dist_cc = distance.cdist(pts_curr, pts_curr, metric='euclidean')
                    dist_cc[range(n_curr), range(n_curr)] = np.inf  # avoid comparing points to themself

                    # indices to the closest point in the current frame
                    idxs_cc = np.argmin(dist_cc, axis=1)

                    # a link is 'correct' if exceeds a series of conditions

                    # the distance ratio criteria takes into account both the distances
                    # to the points in the next frame as well as to points within
                    # the same frame
                    dist_ca_1 = dist_ca[range(n_curr), idxs_ca_1]  # distance to the closest active point
                    dist_ca_2 = dist_ca[range(n_curr), idxs_ca_2]  # distance to the second closest active point
                    dist_cc = dist_cc[range(n_curr), idxs_cc]  # distance to the closest current point
                    dist_ratio = dist_ca_1 / (np.minimum(dist_ca_2, dist_cc) + 2**-23)
                    dist_ratio_flag = (dist_ratio < self._dist_ratio_thr)

                    # compute the mean distance between consecutive points in the active
                    # tracks. This criteria takes into account the absolute distance
                    mean_dist_active = []
                    for i in idx_as_tracks:
                        tr_pts = tracks[i]['pt']
                        # If we have empty observations, we predict positions to calculate the track mean distance
                        if ma.masked in tr_pts:
                            # get the state prediction of the kalman filter and use that predictions like real points
                            state_pred, cov_pred = tracks[i]['kf'].filter(tr_pts)
                            for j, pt in enumerate(tr_pts):
                                if ma.masked in pt:
                                    tr_pts[j] = np.reshape(state_pred[j], (1,4))[:,:2]
                        mean_dist_active.append(self.tr_pts_mean_distance(np.array(tr_pts)))
                    mean_dist_active = np.array(mean_dist_active)
                    mean_dist_thr = np.maximum(self._min_dist, self._mean_dist_thr * mean_dist_active)
                    mean_dist_flag = (dist_ca_1 < mean_dist_thr[idxs_ca_1])
                    min_dist_flag = mean_dist_flag

                    # values in the connected_flag array are True for points that meet
                    # the two criteria above (distance ratio and min distance)
                    connected_flag = np.bitwise_and(dist_ratio_flag, min_dist_flag)

                    # What follows ensures that each active track can be choosen only
                    # once. Think on three aligned points in the plane, with the middle
                    # one corresponding to a current point and the other two to points
                    # on the active points. It might happen that these two points might
                    # "choose" to be connected to the middle one. In this case, we take
                    # a conservative approach and start new tracks from the current
                    # frame point and mark an empty observation to the other two.

                    # indexes to the active short tracks to which points in the current frame
                    # are suppose to be connected
                    idxs_as = [idx_as_tracks[idxs_ca_1[i]] for i in np.where(connected_flag)[0]]

                    # indexes to the active short tracks that where "chosen" by more than a
                    # single point in the current frame, ie. the connection is ambiguous
                    unique, counts = np.unique(idxs_as, return_counts=True)
                    ambiguous = [unique[i] for i, cnt in enumerate(counts) if cnt > 1]

                    # update track list
                    idx_as_tracked = []
                    for i, connected in enumerate(connected_flag):
                        if connected:
                            j = idx_as_tracks[idxs_ca_1[i]]
                            if j in ambiguous:
                                # start new tracks
                                tracks.append(self.new_track_kalman(t, pts_curr[i], init_cov_mat))
                            else:
                                # Is correct to connect that track to a current point
                                tracks[j] = self.update_track_kalman(tracks[j], t, pts_curr[i])
                                idx_as_tracked.append(j)
                        else:
                            # if a point not pass the criteria start a new track
                            tracks.append(self.new_track_kalman(t, pts_curr[i], init_cov_mat))

                    # update active short tracks that not join any point
                    for idx in idx_as_tracks:
                        if idx not in idx_as_tracked:
                            # Update track with an empty medition
                            tracks[idx] = self.update_track_kalman(tracks[idx], t, empty_obs)
            else:
                if n_curr:
                    # if there are not tracks active and there are current points, start new tracks
                    for pt in pts_curr:
                        tracks.append(self.new_track_kalman(t, pt, init_cov_mat))

            print('Analalyzed frames: {}'.format(t))

        # clear tracks (from empty observations)
        for tr in tracks:
            # change empty middle meditions with predicted states
            meditions = tr['pt']
            if meditions.shape[0] > 1:
                states_pred, cov_pred = tr['kf'].filter(meditions)
                for i in tr['idx_inactives']:
                    tr['pt'][i] = states_pred[i,:2]

            # delete last empty points (because the track ended before)
            len_tr = len(tr['t'])
            idx_to_del = [len_tr - i - 1 for i in range(tr['inactive'])]
            tr['t'] = np.delete(tr['t'], (idx_to_del), axis=0)
            tr['pt'] = np.delete(tr['pt'], (idx_to_del), axis=0)
            tr['idx_inactives'] = tr['idx_inactives'][:-tr['inactive']]

        return tracks

    @staticmethod
    def new_track_kalman(time, pt, init_cov_mat):
        """
        Create a track, wich have a kalman filter with constant velocities
        """
        trans_matrix = np.array([[1, 0, 1, 0], [0, 1, 0, 1], [0, 0, 1, 0], [0, 0, 0, 1]])
        obs_matrix = np.array([[1, 0, 0, 0], [0, 1, 0, 0]])
        initial_state = ma.array((pt[0],pt[1], 1, 1))
        kf = KalmanFilter(transition_matrices=trans_matrix,
                          observation_matrices=obs_matrix,
                          initial_state_mean=initial_state,
                          initial_state_covariance=init_cov_mat)

        return {'t': [time,],
                'pt': ma.array([(pt[0], pt[1])]),
                'inactive': 0,
                'kf': kf,
                'state_mean': initial_state,
                'cov_mat': init_cov_mat,
                'idx_inactives': []}

    @staticmethod
    def update_track_kalman(tr, time, pt):
        """
        Return the track updated with a new time and point
        """
        pt = ma.reshape(pt, (1,2))
        tr['t'].append(time)
        tr['pt'] = ma.concatenate((tr['pt'], pt), axis=0)

        # if pt is a miss observation, update track information relative to inactive
        if ma.masked in pt:
            tr['idx_inactives'].append(len(tr['t']) - 1)
            # theres no current points to join, so the time inactive field add one
            tr['inactive'] += 1

        # update kalman filter with the new observation
        state_pred, cov_pred = tr['kf'].filter_update(filtered_state_mean=tr['state_mean'],
                                                      filtered_state_covariance=tr['cov_mat'],
                                                      observation=ma.reshape(pt, (2,)))

        # update track state_mean and cov_mat with the new prediction
        tr['state_mean'] = state_pred
        tr['cov_mat'] = cov_pred
        return tr

    @staticmethod
    def get_idx_asl_tracks(tracks, max_tgap, kf_long):
        """
        Get index of all track that are active (at the end of a track there are
        not more than max_tgap missed observations continuously) and separate
        active tracks in short or long with kf_long threshold (taking into
        account the length of the track as the number of real observations)
        """
        idx_as_tracks = []
        idx_al_tracks = []
        for i, tr in enumerate(tracks):
            is_active = tr['inactive'] < max_tgap
            if is_active:
                # get number of active points in the track
                n_active_points = len(tr['t']) - len(tr['idx_inactives'])
                # separate short track and long tracks
                idx_as_tracks.append(i) if n_active_points <= kf_long else idx_al_tracks.append(i)
        return idx_as_tracks, idx_al_tracks

    @staticmethod
    def tr_pts_mean_distance(points):
        """
        Mean distance between consecutive points
        """
        points = np.atleast_2d(points)
        if points.shape[0] < 2:
            return 0.0
        P, Q = points[1:, :], points[:-1, :]
        return np.median(np.sqrt(np.sum((P - Q) ** 2.0, axis=1)))

    def _link_points(self, detections, n_frames):
        """
        Link detections using a 'distance to other detections' criteria
        """
        tracks = []
        for t in range(n_frames):
            # coordinates of the points in frame t
            pts_curr = np.atleast_2d(detections[t])

            # This is because if there are no detections, pts_curr is a
            # list with an empty array
            n_curr = len(pts_curr) if len(pts_curr[0]) else 0

            # no track => every point in the current frame sets a new track
            if not tracks and n_curr != 0:
                tracks += [self.new_track(t, pt) for pt in pts_curr]
                continue

            if n_curr:
                # indices to active tracks, ie. those that have their last
                # point in the previous frame
                active_idxs = [i for i, tr in enumerate(tracks) if tr['t'][-1] == (t-1)]
                n_active = len(active_idxs)

                # no track is active => every point in the current frame
                # sets a new track
                if n_active == 0:
                    tracks += [self.new_track(t, pt) for pt in pts_curr]
                    continue

                # coordinates of the last point in the active tracks
                pts_active = np.atleast_2d([tracks[i]['pt'][-1] for i in active_idxs])

                # distance between current and active points. shape=(n_curr, n_active)
                dist_ca = distance.cdist(pts_curr, pts_active, metric='euclidean')

                # Separate cases when there are lot of active points and there are only one
                if n_active > 1:
                    # for each row (point in the current frame), indices to the closest point
                    # and second closest point in the active list
                    idxs_ca_1, idxs_ca_2 = np.argpartition(dist_ca, 1)[:, :2].T
                else:
                    # if there are only one active point, supose that have a second
                    # closest point with an infinite distance
                    idxs_ca_1 = np.argpartition(dist_ca, 0)[:, :1].T[0]
                    idxs_ca_2 = np.zeros((n_curr,), dtype=np.int32) + 1
                    dist_ca2 = np.zeros((n_curr, n_active)) + np.inf
                    dist_ca = np.concatenate((dist_ca,dist_ca2), axis=1)

                # distance between points in the current frame. shape=(n_curr, n_curr)
                dist_cc = distance.cdist(pts_curr, pts_curr, metric='euclidean')
                dist_cc[range(n_curr), range(n_curr)] = np.inf  # avoid comparing points to themself

                # indices to the closest point in the current frame
                idxs_cc = np.argmin(dist_cc, axis=1)

                # a link is 'correct' if exceeds a series of conditions

                # the distance ratio criteria takes into account both the distances
                # to the points in the previous frame as well as to points within
                # the same frame
                dist_ca_1 = dist_ca[range(n_curr), idxs_ca_1]  # distance to the closest point in the prev. frame
                dist_ca_2 = dist_ca[range(n_curr), idxs_ca_2]  # distance to the sencond closest point in the prev. frame
                dist_cc = dist_cc[range(n_curr), idxs_cc]  # distance to the closest point in the current frame
                dist_ratio = dist_ca_1 / (np.minimum(dist_ca_2, dist_cc) + 2**-23)
                dist_ratio_flag = (dist_ratio < self._dist_ratio_thr)

                # compute the mean distance between consecutive points in the active
                # tracks. This criteria takes into account the absolute distance
                # between the points
                mean_dist_active = np.array([self.tr_pts_mean_distance(tracks[i]['pt']) for i in active_idxs])
                mean_dist_thr = np.maximum(self._min_dist, self._mean_dist_thr * mean_dist_active)
                mean_dist_flag = (dist_ca_1 < mean_dist_thr[idxs_ca_1])

                # values in the connected_flag array are True for points that meet
                # the two criteria above (distance ratio and distance value below some thresholds)
                connected_flag = np.bitwise_and(dist_ratio_flag, mean_dist_flag)

                # What follows ensures that each active track can be choosen only
                # once. Think on three aligned points in the plane, with the middle
                # one corresponding to an active track and the other two to points
                # on the current frame. It might happen that these two points might
                # "choose" to be connected to the middle one. In this case, we take
                # a conservative approach and start new tracks from the current
                # frame points.

                # indexes to the active tracks to which points in the current frame
                # are suppose to be connected
                idxs_a = [active_idxs[idxs_ca_1[i]] for i in np.where(connected_flag)[0]]

                # indexes to the active tracks that where "chosen" by more than a
                # single point in the current frame, ie. the connection is ambiguous
                unique, counts = np.unique(idxs_a, return_counts=True)
                ambiguous = [unique[i] for i, cnt in enumerate(counts) if cnt > 1]

                # update track list
                for i, connected in enumerate(connected_flag):
                    if connected:
                        j = active_idxs[idxs_ca_1[i]]
                        if j in ambiguous:
                            # two or more tracks try to join same point, so
                            # start a new track with that point
                            tracks.append(self.new_track(t, pts_curr[i]))
                        else:
                            # a valid join
                            tracks[j]['t'].append(t)
                            tracks[j]['pt'].append(pts_curr[i])
                    else:
                        # points that any track try to join start new tracks
                        tracks.append(self.new_track(t, pts_curr[i]))

        for tr in tracks:
            tr['t'] = np.array(tr['t'])
            tr['pt'] = np.atleast_2d(tr['pt'])

        return tracks

    @staticmethod
    def new_track(t, pt):
        """
        Create a track, wich have a kalman filter with vonstant velocities
        """
        # 'idx_inactives' field that is needed for synchronise visualization
        # with the visualization of the tracks created with kalman filter method
        return {'t': [t,], 'pt': [(pt[0], pt[1]),], 'idx_inactives': []}

    def _filter_one_pass(self, tracks):
        graph = self._conectivity_graph(tracks)

        # remove ambigouous
        # for node, edges in graph.iteritems():
        for node, edges in graph.items():
            if len(edges) > 1:
                graph[node] = []

        roots = set(find_roots(graph))
        leaves = set(find_leaves(graph))

        # sub-paths
        paths = []
        for root in roots.difference(leaves):
            paths_from_root = all_paths(graph, root)

            paths += paths_from_root

        # init track list
        tracks_new = []

        # dangling nodes -> tracks without modifying
        for i in roots.intersection(leaves):
            tracks_new.append(tracks[i])

        # join tracks from paths
        for path in paths:
            tr = {}
            tr['t'] = np.concatenate([tracks[i]['t'] for i in path]).astype(np.int32)
            tr['pt'] = np.concatenate([tracks[i]['pt'] for i in path], axis=0)
            tracks_new.append(tr)

        return tracks_new

    def _conectivity_graph(self, tracks):
        ntr = len(tracks)

        # init connectivity graph
        graph = dict((i, []) for i in range(ntr))

        # track start and end times/points
        t_begin = np.array([tr['t'][0] for tr in tracks], dtype=np.int32)
        t_end = np.array([tr['t'][-1] for tr in tracks], dtype=np.int32)
        pt_begin = np.array([tr['pt'][0] for tr in tracks])
        pt_end = np.array([tr['pt'][-1] for tr in tracks])

        # mean distance between consecutive points
        mean_dist = np.array([self.tr_pts_mean_distance(tr['pt']) for tr in tracks])
        #mean_dist = np.maximum(self._min_dist, mean_dist)

        # for i in xrange(ntr):
        for i in range(ntr):
            # wich tracks could paste on right in track[i] because of the time
            t_gap = t_begin - t_end[i]
            within_t_gap = np.bitwise_and(t_gap > 0, t_gap <= self._max_tgap)
            tr2_idxs = np.where(within_t_gap)[0]
            if len(tr2_idxs) == 0:
                continue

            # distance between the last point of i-th and the first one of the
            # tracks that started within the allowed time gap
            dist_i = distance.cdist(np.atleast_2d(pt_end[i]),
                                    np.atleast_2d(pt_begin[tr2_idxs]))[0]
            dist_i_thr = np.sqrt(t_gap[tr2_idxs]) * mean_dist[i]
            # wich tracks that could paste track[i] because of the time, also
            # could paste because of the distance
            dist_i_idxs = np.where(dist_i < dist_i_thr)[0]

            # update edges for node i
            graph[i] = [tr2_idxs[j] for j in dist_i_idxs]

        return graph

if __name__ == "__main__":
    # get defaults from class
    default_param = Linker().get_param()
    parser = argparse.ArgumentParser(
        description='Linker',
        add_help=True,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('file', help='input detections', type=str)
    parser.add_argument('--kalman', help='use kalman filter', default=False, action='store_true')
    parser.add_argument('--min_dist', help='minimum average distance', type=float, default=default_param['min_dist'])
    parser.add_argument('--mean_dist_thr', help='distance threshold (relative to the mean distance between points)', type=float, default=default_param['mean_dist_thr'])
    parser.add_argument('--dist_ratio_thr', help='threshold on the ratio between the distances to first and second closest point in a list', type=float, default=default_param['dist_ratio_thr'])
    parser.add_argument('--max_tgap', help='max time gap between subtracks', type=int, default=default_param['max_tgap'])
    parser.add_argument('--n_dry_run', help='number of runs to set parameters for kalman filter method', type=int, default=default_param['n_dry_run'])
    parser.add_argument('--kf_long', help='long of tracks to start to do predictions with kalman filter', type=int, default=default_param['kf_long'])
    parser.add_argument('--kf_radio', help='distance threshold of kalman filter (relative to distance between predictions and detections)', type=float, default=default_param['kf_radio'])
    parser.add_argument('--n_frames', help='number of frames to get tracks', type=int, default=None)
    parser.add_argument('--filter_min', help='filter tracks with min length (only for visualization)', type=int, default=None)
    parser.add_argument('--view', help='view results', default=argparse.SUPPRESS, action='store_true')
    parser.add_argument('--view_points', help='view medition and predictions points (only for Kalman filter)', default=argparse.SUPPRESS, action='store_true')
    parser.add_argument('--roi', help='process ROI (defined as the 4-tuple (x, y, w, h))', type=str, default=None)
    parser.add_argument('--out', help='output file', type=str)
    args = vars(parser.parse_args())

    # verificate if parameters are ok
    assert(args['kf_long'] > 0)
    if args['kalman'] is False and args['max_tgap'] > 0:
        assert(bool('view_points' not in args))

    # check input file
    srcfile = os.path.abspath(args['file'])
    if not os.path.exists(srcfile):
        raise IOError('{} doesn\'t exists'.format(srcfile))

    # set parameters of the linker and create it
    param = dict([(k, args[k]) for k in default_param.keys()])
    linker = Linker(**param)

    # load detections dictionary
    try:
        with open(srcfile) as file:
            ddict = json.load(file)
        detections = ddict['detections']
    except:
        raise IOError('can\'t read {}'.format(srcfile))

    # filter detections inside the roi if is selected the option '--roi'
    roi = args['roi']
    if roi is not None:
        # get data of the roi
        from ast import literal_eval
        roi = literal_eval(args['roi'])
        x0, y0, x1, y1 = float(roi[0]), float(roi[1]), \
                         float(roi[0] + roi[2]), float(roi[1] + roi[3])

        def is_inside(pt):
            """
            return if pt is inside the roi compose by the points x0,y0,x1,y1
            """
            x, y = pt
            return x >= x0 and y >= y0 and x < x1 and y < y1

        # filter detections inside the roi in each frame
        for i, det in enumerate(detections):
            detections[i] = np.array([pt for pt in det if is_inside(pt)])

    # set number of frames to run the linker
    n_frames = args['n_frames']
    if n_frames is None or n_frames > ddict['frame_count']:
        n_frames = ddict['frame_count']
    print('>> number of frames to calculate trajectories: {}'.format(n_frames))

    tracks = linker.process(ddict['detections'], n_frames)
    print('>> {} tracks'.format(len(tracks)))

    # create track dictionary to save data results
    tdict = {}
    tdict['input_file'] = ddict['input_file']
    tdict['detector_file'] = os.path.split(srcfile)[1]
    tdict['timestamp'] = time.ctime()
    tdict['param'] = param
    tdict['frame_height'] = ddict['frame_height']
    tdict['frame_width'] = ddict['frame_width']
    tdict['fps'] = ddict['fps']
    tdict['total_frames'] = n_frames
    tdict['roi'] = roi
    # clear tracks for save in track dictionary (also transform np.arrays to list
    # for save in json file)
    for track in tracks:
        track.pop('cov_mat', None)
        track.pop('state_mean', None)
        track.pop('kf', None)
        track.pop('inactive', None)
        track['t'] = track['t'].tolist()
        track['pt'] = track['pt'].tolist()
    tdict['tracks'] = tracks

    # destination file
    dstfile = None if 'out' not in args else args['out']
    if dstfile is None:
        # create a destfile name with parameters of the linker
        param = {k: v for k, v in param.items() if default_param[k] != v}
        if len(param) > 0:
            param_str = '-'.join([':'.join((k.lstrip('-'), str(v))) for k,v in param.items()])
            dstfile = os.path.splitext(srcfile)[0] + '-' + param_str + '.tracks'
        else:
            dstfile = os.path.splitext(srcfile)[0] + '.tracks'

    # save the track dictionary in a json file
    with open(dstfile, 'w') as outfile:
        json.dump(tdict, outfile)
    print('results saved to \'{}\''.format(dstfile))

    # filter tracks with filter_min length for visualization
    if args['filter_min'] != None:
        tdict['tracks'] = [tr for tr in tdict['tracks'] if len(tr['t']) >= int(args['filter_min'])]
        print('>> filtered tracks for visualization: {}'.format(len(tdict['tracks'])))

    # visualization
    if bool('view' in args):
        # sort by track lenght
        tlen = [len(t['pt']) for t in tdict['tracks']]
        tdict['tracks'] = [tdict['tracks'][i] for i in np.argsort(tlen)]
        view_points = bool('view_points' in args)
        view(tdict, view_points)
