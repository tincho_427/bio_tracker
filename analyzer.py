# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from matplotlib.cm import jet as cmap
import argparse
import os
import numpy as np
np.seterr(all='raise')
import json
from helpers import *
from chd_detector import *


def view(tdict, view_points=False):
    """
    Draw tracks. If view_points, show also true detections and missing
    detections

    """
    particle_radio = tdict['particle_size'] / 2
    np.random.seed(111)
    tracks = tdict['tracks']
    fig, ax = plt.subplots()
    colors = cmap(np.linspace(0., 1., len(tracks)))
    np.random.shuffle(colors)
    for i, tr in enumerate(tracks):
        x, y = np.array(tr['pt']).T
        y_offset = 0.5 * np.random.random()
        ax.plot(x, y + y_offset, '-', colors[i])
        circ = plt.Circle((x[-1], y[-1] + y_offset), particle_radio,
                          color='black')
        ax.add_patch(circ)

        if view_points:
            # get points in the track that were predicted with kalman filter
            idx_pred = np.array(tr['idx_inactives'])

            # plot detections points
            x_med = np.array([x for i, x in enumerate(x) if i not in idx_pred])
            y_med = np.array([y for i, y in enumerate(y) if i not in idx_pred])
            ax.plot(x_med, y_med + y_offset, 'x', color='b', ms=4)

            # plot predictions points (with kalman)
            x_pred = np.array([x for i, x in enumerate(x) if i in idx_pred])
            y_pred = np.array([y for i, y in enumerate(y) if i in idx_pred])
            ax.plot(x_pred, y_pred + y_offset, 'x', color='r', ms=5)

    ax.axis([0., float(tdict['frame_width'] - 1), 0.,
             float(tdict['frame_height'] - 1)], 'equal', adjustable='box')
    ax.grid()
    plt.gca().invert_yaxis()
    plt.title('tracks')
    plt.show()


def streaming_view(tdict, srcfile):
    """
    Draw tracks in streaming with the video(srcfile)
    """
    import cv2
    _rint = lambda x: int(np.rint(x))

    # read video
    try:
        device = cv2.VideoCapture(srcfile)
        fourcc = device.get(cv2.CAP_PROP_FOURCC)
        fps = device.get(cv2.CAP_PROP_FPS)
        video_height = device.get(cv2.CAP_PROP_FRAME_HEIGHT)
        video_width = device.get(cv2.CAP_PROP_FRAME_WIDTH)
    except:
        raise IOError('can\'t read {}'.format(filename))

    if device.get(cv2.CAP_PROP_FRAME_COUNT) == 0:
        raise IOError('can\'t open {}'.format(srcfile))

    # read screen info to get size visualization
    try:
        import screeninfo
        visu_scale = float(screeninfo.get_monitors()[0].height - 128) / \
        float(tdict['frame_height'])
        print('>> visualization scale adjusted for monitor height')
    except:
        visu_scale = 800. / float(tdict['frame_height'])
        print('>> visualization scale adjusted for defect')

    video_width = int(video_width * visu_scale)
    video_height = int(video_height * visu_scale)
    outfile = os.path.splitext(srcfile)[0] + '-streaming_view' + os.path.splitext(srcfile)[1]
    out_video = cv2.VideoWriter(outfile, int(fourcc), float(fps), (video_width, video_height))

    tracks = tdict['tracks']

    # assign one color for each track
    from matplotlib.cm import jet_r as cmap
    from matplotlib import cm
    colors = cmap(np.linspace(0., 1., len(tracks))) * 255
    np.random.seed(111)
    np.random.shuffle(colors)

    t = 0
    # list of last two points for each track until time t
    last_2_pts = [[] for tr in tracks]
    while True:
        ret, frame = device.read()
        if not ret:
            break

        # resize frame
        visu_img = cv2.resize(frame / frame.max(),
                          dsize=(_rint(visu_scale * frame.shape[1]),
                                 _rint(visu_scale * frame.shape[0])))
        if t == 0:
            #  create an empty image to draw tracks in it
            visu_lines = np.zeros(visu_img.shape)
            radius = max(1, int(visu_scale * 1.4142 * 4))

        # if track have time t, transform corresponding point with visual scale
        # and update last_2_pts. Otherwise delete data of track (because its dead)
        for i, tr in enumerate(tracks):
            idxs_t = np.where(np.array(tr['t']) == t)[0]
            if len(idxs_t):
                last_2_pts[i].append(visu_scale * np.array(tr['pt'][idxs_t[0]]))

        # draw line tracks, ie previous frames points
        for i, pts in enumerate(last_2_pts):
            if len(pts) > 1:
                cv2.polylines(visu_lines, np.int32([pts]), 0, colors[i], thickness=1)
                # delete starting point
                del last_2_pts[i][0]

        # draw circles to the particles in the current frame
        visu_circles = np.array(visu_lines)
        for i, pts in enumerate(last_2_pts):
            if t in tracks[i]['t']:
                cv2.circle(visu_circles,
                (int(pts[-1][0]), int(pts[-1][1])),
                radius=radius, color=(255, 255, 255), thickness=1)

        # shuffle video frame with the image with tracks
        alpha = 0.6
        visu = alpha * visu_circles + (1 - alpha) * (255.* visu_img)
        visu = visu.astype('uint8')

        cv2.imshow("Tracks", visu)
        out_video.write(visu)

        ch = cv2.waitKey(1)
        if ch & 0xFF == 27 or ch & 0xFF in (ord('q'), ord('Q')):
            break

        t += 1
    device.release()
    out_video.release()
    cv2.destroyAllWindows()

def streaming_view_individual(tdict, srcfile):
    """
    Draw a track in streaming in an adjust window. Only see the time video on
    the time of life of the track.
    """
    import cv2
    _rint = lambda x: int(np.rint(x))

    # check if you have only one track
    if len(tdict['tracks']) > 1:
        raise IOError('there are too many tracks selected: {}. Select one with \'samples\' option or \'n_samples\' option'.format(len(tdict['tracks'])))
    track = tdict['tracks'][0]

    # read video
    device = cv2.VideoCapture(srcfile)
    if device.get(cv2.CAP_PROP_FRAME_COUNT) == 0:
        raise IOError('can\'t open {}'.format(srcfile))

    # set the time to see of the video with a padding time of 10 frames
    min_t = (track['t'][0] - 10) if track['t'][0] > 10 else 0
    max_t = (track['t'][-1] + 10) if track['t'][-1] < (device.get(cv2.CAP_PROP_FRAME_COUNT) - 10) else device.get(cv2.CAP_PROP_FRAME_COUNT)
    device.set(cv2.CAP_PROP_POS_FRAMES, min_t)

    # get coordinates of a box around the track
    x_1, y_1 = np.min(track['pt'], axis=0)
    x_2, y_2 = np.max(track['pt'], axis=0)
    # add padding to the box if its possible
    padding = 20
    x_1_pad = int(x_1 - padding) if x_1 - padding >= 0 else 0
    y_1_pad = int(y_1 - padding) if y_1 - padding >= 0 else 0
    x_2_pad = int(x_2 + padding) if x_2 + padding < tdict['frame_width'] else tdict['frame_width']
    y_2_pad = int(y_2 + padding) if y_2 + padding < tdict['frame_height'] else tdict['frame_height']

    # read screen info to get size visualization
    try:
        import screeninfo
        if ((y_2_pad - y_1_pad) > (x_2_pad - x_1_pad)):
            visu_scale = float(screeninfo.get_monitors()[0].height - 128) / \
            float(y_2_pad - y_1_pad)
            print('>> visualization scale adjusted for monitor height')
        else:
            visu_scale = float(screeninfo.get_monitors()[0].height - 128) / \
            float(x_2_pad - x_1_pad)
            print('>> visualization scale adjusted for monitor width')
    except:
        visu_scale = 800. / float(y_2_pad - y_1_pad)
        print('>> visualization scale adjusted for defect')

    last_2_pts = [] # to draw lines between each tracks
    for t in range(int(min_t), int(max_t)):
        ret, frame = device.read()
        # crop the frame around the track and scale it for visualization
        frame = frame[y_1_pad:y_2_pad, x_1_pad:x_2_pad, :]
        visu_img = cv2.resize(frame / frame.max(),
                          dsize=(_rint(visu_scale * frame.shape[1]),
                                 _rint(visu_scale * frame.shape[0])))
        if t == min_t:
            #  create an empty image to draw tracks in it
            visu_lines = np.zeros(visu_img.shape)

        # if track have time t, transform corresponding point with visual scale
        if t in range(track['t'][0], track['t'][-1] + 1):
            last_2_pts.append(visu_scale * (track['pt'][t - track['t'][0]] - np.array([x_1_pad, y_1_pad]))) # scale the pt in the time t

        # draw line tracks, ie previous frames points
        cv2.polylines(visu_lines, np.int32([last_2_pts]), isClosed=0, color=(0,255,255), thickness=1)

        # delete starting point (to draw next line)
        if len(last_2_pts) > 1:
            del last_2_pts[0]

        # shuffle video frame with the image with the track
        alpha = 0.5
        visu = alpha * visu_lines + (1 - alpha) * visu_img

        cv2.imshow("Tracks", visu)

        ch = cv2.waitKey(1)
        if ch & 0xFF == 27 or ch & 0xFF in (ord('q'), ord('Q')):
            break

def view_tumbles(tdict, tumbles, degrees):
    """
    Draw one by one tracks, simplified tracks (obtained with tumbles algorithm)
    and tumble points
    """
    particle_radio = tdict['particle_size'] / 2
    for i, track in enumerate(tdict['tracks']):
        fig = plt.figure()
        plt.rcParams['axes.facecolor'] = 'black'
        ax = fig.add_subplot(111, aspect='equal')

        # draw track
        x, y = np.array(track['pt']).T
        ax.plot(x, y, 'r-')
        # draw simplified track
        simplified_pts = np.vstack((track['pt'][0], tumbles[i], track['pt'][-1]))
        sx, sy = np.array(simplified_pts).T
        ax.plot(sx, sy, 'g--')

        ax.plot(sx[1:-1], sy[1:-1], 'bo', markersize=5)
        circles = plt.Circle((x[-1], y[-1]), particle_radio, color='white')
        ax.add_artist(circles)

        plt.axis([x.min() - 15, x.max() + 15,
                  y.min() - 15, y.max() + 15])
        plt.title('track id:  #' + str(track['id']))
        ax.set_facecolor('black')
        plt.xlabel('μm');
        ax.grid(color='white')
        plt.gca().invert_yaxis()

        # accept 'enter' key for continue with next figure
        def quit_figure(event):
            if event.key == 'enter':
                plt.close(event.canvas.figure)

        cid = plt.gcf().canvas.mpl_connect('key_press_event', quit_figure)
        plt.show()
        if exit:
            continue

def get_cos_histogram(track):
    """
    Calculate a histogram of cos(theta) with degrees between each consecutetive points
    """
    points = track['pt']
    directions = np.diff(points, axis=0)
    theta = degree(directions)
    cos_theta = np.cos(theta)

    histogram_values, edges = np.histogram(cos_theta, 10, range=(-1,1), normed=True)
    return histogram_values, edges

def view_histograms(histogram_values, edges, title, ids):
    """
    Draw one by one histograms
    """
    for i, hist_values in enumerate(histogram_values):
        binWidth = edges[i][1] - edges[i][0]
        # histogram_values = hist_values * binWidth

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.bar(edges[i][:-1] + 0.1, hist_values * binWidth, binWidth, facecolor='red')
        plt.xlabel('degrees')
        plt.ylabel('count')
        plt.title(title + ' - track id:  #' + str(ids[i]))
        ax.set_facecolor('white')
        ax.grid(color='black')
        plt.gca()

        # accept 'enter' key for continue with next figure
        def quit_figure(event):
            if event.key == 'enter':
                plt.close(event.canvas.figure)

        cid = plt.gcf().canvas.mpl_connect('key_press_event', quit_figure)
        plt.show()
        if exit:
            continue

def get_cos_histogram_all2all(track):
    """
    Calculate a histogram of cos(theta) with degrees between each consecutetive points (i.e.
    for all pairs of two vectors, except each one with eachself)
    """
    points = track['pt']
    cos_theta = []
    for i, pt in enumerate(points[:-1]):
        v1 = points[i + 1] - points[i]
        for j, pt2 in enumerate(points[:-1]):
            # not compare with eachself
            if i == j:
                continue
            v2 = points[j + 1] - points[j]
            # calculate degree between the two vectors
            cos_theta.append((v1*v2).sum(axis=0)/(np.sqrt((v1**2).sum(axis=0)*(v2**2).sum(axis=0))))
    histogram_values, edges = np.histogram(cos_theta, 10, range=(-1,1), normed=True)
    return histogram_values, edges

def save_data(dstfile, tracks, tumbles_det):
    tracks_data = []
    tumbles, degrees = tumbles_det.get_tumbles(tdict['tracks'])

    chd_x = []
    chd_y = []
    for tumbles_track in tumbles:
        if len(tumbles_track):
            chd_x.append([tumble[0] for tumble in tumbles_track])
            chd_y.append([tumble[1] for tumble in tumbles_track])
        else:
            chd_x.append([])
            chd_y.append([])
    for i, tr in enumerate(tracks):
        x, y = np.array(tr['pt']).T
        lap = len(tr['t'])
        length = get_distance_traveled(tr)
        tr_data = {'id': tr['id'],
                   'timestamp': tr['t'],
                   'pt_x': x.tolist(),
                   'pt_y': y.tolist(),
                   'points': len(tr['pt']),
                   'lap': lap,
                   'L': get_L(tr),
                   'length': length,
                   'vrel': get_vrel(tr),
                   'speed': length / lap,
                   'theta': get_mean_degree_b3pts(tr),
                   'num_chd': len(tumbles[i]),
                   'chd_x': chd_x[i],
                   'chd_y': chd_y[i],
                   'degrees': degrees[i].tolist(),
                   'timestamp_chd': get_times_from_tumbles(tr, tumbles[i])
                   }
        tracks_data.append(tr_data)

    with open(dstfile, 'w') as outfile:
        json.dump(tracks_data, outfile)
    print('data saved to \'{}\''.format(dstfile))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Analyzer',
        add_help=True,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('file', help='input file', type=str)
    # configure options
    parser.add_argument('--particle_size', help='(conf) Diameter of the particle in pixels', type=int, default=None)
    parser.add_argument('--min_len', help='(conf) filter tracks with min length', type=int)
    parser.add_argument('--min_len_nbodies', help='(conf) filter particles with motion big than n_bodies at least in one dimension', type=float)
    parser.add_argument('--dead', help='process also dead cells', default=argparse.SUPPRESS, action='store_true')
    parser.add_argument('--save_config', help='save tracks with (conf) options', default=argparse.SUPPRESS, action='store_true')
    # filter samples options
    parser.add_argument('--samples', help='filter tracks by ids: [id1,id2,...] (not spaced)', type=str, default=None)
    parser.add_argument('--n_samples', help='filter only n_samples random tracks', type=int)
    # view options
    parser.add_argument('--view', help='view tracks', default=argparse.SUPPRESS, action='store_true')
    parser.add_argument('--view_points', help='view medition and predictions points', default=argparse.SUPPRESS, action='store_true')
    parser.add_argument('--streaming_view', help='view tracks in streaming. Video input', type=str, default=None)
    parser.add_argument('--streaming_view_individual', help='view a track in streaming(select it with --samples [id]). Video input', type=str, default=None)
    # analyzer options
    parser.add_argument('--epsilon', help='epsilon for rdp algorithm used for detect changes of directions', type=float, default=10.0)
    parser.add_argument('--n_bodies', help='number of bodies to consider change of direction', type=float, default=2)
    parser.add_argument('--min_degree', help='min degree of a change direction', type=int, default=0)
    parser.add_argument('--max_degree', help='min degree of change direction', type=int, default=180)
    parser.add_argument('--view_chd', help='view change of directions', default=argparse.SUPPRESS, action='store_true')
    #  Save data options
    parser.add_argument('--out', help='optional name of output file where save the data', type=str)
    parser.add_argument('--save_data', help='Save data of each track', default=argparse.SUPPRESS, action='store_true')
    args = vars(parser.parse_args())

    # read data
    filename = os.path.abspath(args['file'])
    try:
        with open(filename) as file:
            tdict = json.load(file)
    except:
        raise IOError('can\'t read {}'.format(filename))

    # if track doesn't have ID, add it and save the new file
    if 'id' not in tdict['tracks'][0].keys():
        for i, tr in enumerate(tdict['tracks']):
            tr['id'] = i
        with open(filename, 'w') as outfile:
            json.dump(tdict, outfile)
        print('Ids aggregated. Results saved to \'{}\''.format(filename))

    # ----- Config options -----

    # set the particle size
    if args['particle_size'] is not None:
        tdict['particle_size'] = args['particle_size']
        print('>>> Particle size was set')
    elif 'particle_size' not in tdict.keys():
        # if particle_size is not set, use default options
        tdict['particle_size'] = 5
        print('>>> Particle size set default: {}'.format(tdict['particle_size']))

    # filter tracks with min length of points
    if args['min_len']:
        tdict['tracks'] = [tr for tr in tdict['tracks'] if len(tr['pt']) >= args['min_len']]
        print('>>> Filter tracks with min length {}'.format(args['min_len']))

    # only tracks with a motion bigger than n_bodies at least in one dimension
    if args['min_len_nbodies']:
        tdict['tracks'] = [tr for tr in tdict['tracks'] if
                              move_at_least_nbodies(tr,
                                                    tdict['particle_size'],
                                                    args['min_len_nbodies'])]
        print('>>> Filter tracks with motion bigger than {} bodies(at least in one dimension)'.format(args['min_len_nbodies']))

    # filter out dead cells
    if 'dead' not in args:
        std_thr = 0.5
        std = [mad(np.array(tr['pt']), axis=0) for tr in tdict['tracks']]
        idxs = np.where(np.max(std, axis=1) > std_thr)[0]
        tdict['tracks'] = [tr for i, tr in enumerate(tdict['tracks']) if i in idxs]

    # overwrite the file with config options
    if 'save_config' in args:
        with open(os.path.splitext(filename)[0] + '-conf.tracks', 'w') as outfile:
            json.dump(tdict, outfile)
        print('>> Configuration options saved to \'{}\''.format(filename))

    # ----- Filter samples options -----

    # filter only the tracks with the ids in the list of samples
    if args['samples'] is not None:
        from ast import literal_eval
        samples = literal_eval(args['samples'])
        tdict['tracks'] = [track for track in tdict['tracks'] if track['id'] in samples]

    # filter only n_samples random tracks
    if args['n_samples']:
       idxs = np.random.randint(0, len(tdict['tracks']), args['n_samples'])
       tdict['tracks'] = [tdict['tracks'][i] for i in idxs]

    print('>> {} tracks'.format(len(tdict['tracks'])))

    # ----- View options -----

    if 'view' in args:
        view(tdict)

    if 'view_points' in args:
        view(tdict, True)

    if args['streaming_view'] is not None:
        videoname = os.path.abspath(args['streaming_view'])
        streaming_view(tdict, videoname)

    if args['streaming_view_individual'] is not None:
        videoname = os.path.abspath(args['streaming_view_individual'])
        streaming_view_individual(tdict, videoname)

    # ----- Analyze options

    if 'view_chd' in args:
        print('>> showing degrees between {}° and {}°'.format(args['min_degree'], args['max_degree']))
        print('>> considering {} number of bodies of min distance between tumbles'.format(args['n_bodies']))
        # DELETE: put min_dist = n_bodies * particle_size: VER EL QUILOMBO DE PIXEL Y PARTICLE SIZE: POR EJEMPLO el campo min_dist cuando se crea el CHDdetector
        tumbles_det = CHDDetector(min_degree=args['min_degree'],
                                  max_degree=args['max_degree'],
                                  epsilon=args['epsilon'],
                                  min_dist=(args['n_bodies'] * tdict['particle_size']))
        tumbles, degrees = tumbles_det.get_tumbles(tdict['tracks'])
        view_tumbles(tdict, tumbles, degrees)

    #  ----- Save data options -----

    if 'save_data' in args:
        dstfile = None if 'out' not in args else args['out']
        if dstfile is None:
            dstfile = os.path.splitext(filename)[0] + '-data.json'
        tumbles_det = CHDDetector(min_degree=args['min_degree'],
                                  max_degree=args['max_degree'],
                                  epsilon=args['epsilon'],
                                  min_dist=(args['n_bodies'] * tdict['particle_size']))
        save_data(dstfile, tdict['tracks'], tumbles_det)
