# Particle Tracking Software

This project implement a particle tracking system in microscopy videos. The task is divided in three modules: detection, linker and analyzer.

1. The detection module is mainly in charge of clean images from static noise belong the frames, and from Gaussian noise, and then obtain the coordinates of the particles in each frame using a local maxima after applying a Laplacian filter.

2. The linker module is in charge to link the coordinates detection over the time. For this task the linker use Kalman filters to predict possible links and take decision of correct links.

3. The analyzer module perform some data extraction functions.

---
## SETUP

There are two ways to install the project: with docker or by yourself. It's recommended to use docker

### Docker

Clone the [repository for install docker for this project](https://bitbucket.org/tincho_427/docker_bio_tracker/src/master/) and follow the instructions in the readme.

#### Run the docker container
When you want to run the project you should run the docker container:
```
./run_docker
```

When you want to exit type ```ctrl```+```d```.


### By yourself

Upgrade any pre-installed packages:
```sudo apt-get update```

Install requirements for OpenCV:
```
sudo apt-get install build-essential cmake git pkg-config
sudo apt-get install libjpeg8-dev libtiff4-dev libjasper-dev libpng12-dev
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get install libgtk2.0-dev
sudo apt-get install libatlas-base-dev gfortran
sudo apt-get install python3-tk
```


For installation you need to have pip3 installed. For install it:
```
sudo apt install python3-pip
```

Install the developer packages for Python3.5:
```sudo apt-get install python3.5-dev```


Install OpenCV:
```
pip3 install
```

Download and install OpenCV:
```
cd ~ && git clone https://github.com/Itseez/opencv.git
cd ~ && git clone https://github.com/Itseez/opencv_contrib.git
cd ~/opencv && mkdir build && cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE \
	-D CMAKE_INSTALL_PREFIX=/usr/local \
	-D INSTALL_C_EXAMPLES=ON \
	-D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules \
	-D PYTHON_DEFAULT_EXECUTABLE=$(which python3) \
        -D WITH_CUDA=OFF ..
make -j4
sudo make install
sudo ldconfig
```

For test if installation is ok run:
```
python3
>>> import cv2
```

Clone the repository and install requirements:
```
git clone git clone https://tincho_427@bitbucket.org/tincho_427/bio_tracker.git
cd bio_tracker
pip3 install -r requirements.txt
mkdir build
cd build
cmake ..
make
cd ..
```

---
## Running Tests

You need to install nose. If you run the project in the docker containera is already installed. If you run the project by yourself you need to install it:
```pip3 install nose```

For run tests:
``` ./run_tests ```

---
## Detector

**Parameters:**
('Bool' type it could be: True/False, 1/0, yes/no, on/off)

  * **--prescale** *Float*: scale to work images. It's used to reduce computing time.
  * **--avg** *Bool*: subtraction average image to reduce static noise (dead particles, stain produces by the fluid, etc.)
  * **--n** *Int*: number of random frames to use to calculate average image ('**--avg**' option have to be 'True'). Value 0 correspond to all frames.
  * **--nlmeans** *Bool*: filter frames with non-local means algorithm to reduce Gaussian noise. It's recommended to apply only when the noise level is big because it's very expensive.
  * **--sigma** *Float*: standard deviation use in Gaussian blur. It's responsible for establishing the size of the particles to be detected. The radius of the particles should be of sqrt(2) σ (taking into account that we are talking in pixel level).
  * **--invert** *Bool*: work with inverted colors. Because of the maxima local search and for the video to analyze it could be necessary to invert colors.
  * **--thr** *Float*: threshold use after local maxima to separate peaks produced by particles and peaks produced by noise. It depends of how much clean are the images after applying Gaussian blur. Usually values between 0.5-0.7 works fine.
  * **--subpix** *Bool*: fit a paraboloid to get more precise coordinates. If this option is used, coordinates stop being Integer (pixels).
  * **--view**: show the video with the detections marked.
  * **--out** *String*: path and name to save detections file. If this option isn't set, the file name use in the detector parameters.

**Default options:**

* prescale = 1.0
* avg = True
* n = 0
* nlmeans = False
* sigma=1.5
* invert = False
* thr = 0.6
* subpix = True

It's recomended to follow a guide to modify parameters:

* **sigma** approximated -> **invert** -> **avg** (and **n**) -> **prescale** (know that **sigma** scale with this) -> **sigma** -> **thr**

* **nlmeans** and **subpix** is recommended not modify.

* In the configuration of parameter stage, is recommended to maintain the **--view** option to show results. In any moment you could press 'q' key to stop the detector. When configurations are fine, it's recommended to disable this option because it's expensive.


### Examples

1. ```python3 detector.py 'videos/particles.avi' --view --sigma 5.5```

    If this takes too much time, it could be necessary to resize images to process images faster. It's not recommended to reduce too much the size if images or particles are too small. Suppose that's not the case, so we could run this:

    ```python3 detector.py 'videos/particles.avi' --view --prescale 0.8 --sigma 6.875```

2. ```python3 detector.py 'videos/particles.avi' --view --sigma 5.5```

    If the size of the detection are good, it could be that detections seems to be at the contour of the real particle. In this case, the problem is in the colours. You have to invert them:

    ```python3 detector.py 'videos/particles.avi' --view --sigma 5.5 --invert 1```

3. ```python3 detector.py 'videos/particles.avi' --view --sigma 5.5 --invert 1```

    If particles detections are fine, but there are false detections too, the problem is after trying to clean the images, they clean continue having noise. It's possible if the noise is small that could fix it modifying '*--thr*' parameter:

    ```python3 detector.py 'videos/particles.avi' --view --sigma 5.5 --invert 1 --thr 0.5```

    If this don't work you could try to apply ```--nlmeans 1``` option.

---
## Linker

**Notation to understand parameters:**

  * *dist_cc*: concern to the distance of a point in the current frame (corresponding to the frame analyzed at moment) to the closest point in the same frame.
  * *dist_ca_1*: concern to the distance of a point in the current frame (corresponding to the frame analyzed at moment) to the closest track formed to the moment, i.e. the distance to the last point of the track.
  * *dist_ca_2*: concern to the distance of a point in the current frame (corresponding to the frame analyzed at moment) to the second closest track formed to the moment.


**Parameters:**
('Bool' type it could be: True/False, 1/0, yes/no, on/off)

  * **--max_tgap** *Int*: maximum number of video frames where there may be missed detections and the track continue.
  * **--dist_ratio_thr** *Float*: threshold to check one of the conditions for determinate if a possible link is nice in the 'distance to other detections' criteria. For that, check if ```(dist_ca_1 / min(dist_ca_2, dist_cc)) < dist_ratio_thr```.
  * **--min_dist** *Float*: min distance that a particle could 'jump' without taking into account the previous behavior.
  * **--mean_dist_thr** *Float*: acceleration factor that a particle could have respect to the average jump distances up to the observed moment.
  * **--kalman**: apply the linker particle method with Kalman filters. If this parameter is not set, the linker is done with the 'distance to other particles' method.
  * **--n_dry_run** *Int*: number of frames to pre-run the algorihm to obtain an initial covariance matrix that adjust better data. (Concer to Kalman filters)
  * **--kf_long** *Int*: number of real detections that tracks needs to have at least to stop using the linker method of 'distances to other detectons' and start to use the predictions of the Kalman filters. (It is necessary to collect certain information for good predictions)
  * **--kf_radio** *Float*: search radius around the predictions used to decide whether a detection continue to a track or not.
  * **--n_frames** *Int*: number of frames where apply linker method. If this parameter is not set, the linker is applying over the entire video. It's recommended to use a reduce number of frames to check other parameters.
  * **--filter_min** *Int*: after get tracks with linker method, filter tracks with at leas *filter_mean* points. ONLY for visualization. So, all the tracks save in the output file.
  * **--view**: show tracks. (If there are a lot of tracks it's recomended to set the *--filter_min* option.
  * **--view_points**: only use if *--view* option and *--kalman* are seted. Show each point of the tracks with blue cross (real detections) or red cross (predictions of the Kalman filter).
  * **--roi** *String*: link only detections inside this roi. The value is a String '(x,y,w,h)' where 'x' and 'y' are the coordinates of the initial point of the roi, and 'w' and 'h' are width and height.
  * **--out** *String*: path and name to save linkers file. If this option isn't set, the file name use in the linker parameters.

For more details of the parameters, see the [final work of the carrer](https://rdu.unc.edu.ar/handle/11086/6013). In particular, Section 3.2.2.

**Default options:**

* kalman = false
* min_dist = 5.0
* mean_dist_thr = 2.0
* dist_ratio_thr = 0.5
* max_tgap = 3
* n_dry_run = 50
* kf_long = 8
* kf_radio = 5

It's recomended to follow a guide to modify parameters for the linker method with Kalman filters and without it:

* for **kalman**: **min_dist** -> **mean_dist_thr** -> **dist_ratio_thr** -> **kf_long** and **kf_radio** -> **max_tgap** -> adjust again all the parameters to get better results -> **n_dry_run**

* **NOT** kalman: **min_dist** -> **mean_dist_thr** -> **dist_ratio_thr** -> **max_tgap** -> adjust again all the parameters to get better results

* In the configuration of parameter stage, is recommended to maintain the **--view** option to show results and run with **--n_frames** around the value of 100~150.

### Examples

1. ```python3 linker.py 'videos/particles.detections' --view --view_points --kalman --n_frames 100```

2. ```python3 linker.py 'videos/particles.detections' --view --n_frames 100 --min_dist 9.0 --mean_dist_thr 8.0 --max_tgap 3```

3. ```python3 linker.py 'videos/particles.detections' --view --n_frames 100 --roi '(100,100,300,200)'```

---
## Analyzer

**Parameters:**

Configure options:

  * **--particle_size** *Int*: particle diameter in pixels.
  * **--min_len** *Int*: filter tracks with min length points.
  * **--min_len_nbodies** *float*: filter tracks that moves at least a min number of bodies.
  * **--dead**: process also dead cells. Note that filter dead cells if you don't use this option.
  * **--save_config**: save a new file, simil the input file, but with new keys: *'id'*, *'particle_size'*, and only with tracks filtered by the *--min_len*, *--min_len_nbodies* and *--dead* (if their options are set). This file have name equal to the input file, but add it a "-conf.tracks" at the end.

Filter sample options:

  * **--samples** *String*: filter tracks by ids. Note that is a String!!  For example: '[id1,id2,id3,..]'
  * **--n_samples** *Int*: filter a number of random tracks.

View options:

  * **--view**: draw tracks in a clear image.
  * **--view_points**: draw trajectories and points of each track. Where blue crosses are the real detections, and red crosses the predictions of Kalman filter where not detections where found.
  * **--streaming_view** *String*: path to the video file. This automatically activate a streaming view of tracks over the video.
  * **--streaming_view_individual** *String*: path to the video file. Simil to *--streaming_view* option but in the input needs to only have one track. So, the video is cut to see only that track in a zoom video.

Analyzer options:

  * **--epsilon** *Float*: threshold used in change direction detection algorithm. Epsilon used in the steps where apply rdp algorithm.
  * **--n_bodies** *Float*: number of min bodies that a particle needs to move for consider a change of direction.
  * **--min_degree** *Int*: min degree to consider change of direction a point.
  * **--max_degree** *Int*: max degree to consider change of direction a point.
  * **--view_chd**: show track by track with change of directions (you could pass one by one with "Enter" keyboard). It's recommended to apply only with few tracks.

Save data options:

  * **--out** *String*: path and name to save linkers file. If this option isn't set, the file  where save data have the same name of the input file but with a '-data.json' at the end.
  * **--save_data**: save data of tracks in a json file.

**Default options:**

* particle_size = 5
* epsilon = 10.0
* n_bodies = 2
* min_degree = 0
* max_degree = 180

It's recomended to set first this options (default options) to show change of directions detected are nice, and then use the options for save data.

### Examples

1. ```python3 analyzer.py 'videos/particles.tracks' --particle_size 5 --min_len 50```

    This example don't do nothing. Only set the particle diameter and filter tracks with at least 50 points.

2. ```python3 analyzer.py 'videos/particles.tracks' --particle_size 5 --min_len 50 --save_config```

    This example set the same options of the previous example but save the particle size option and the filter tracks in 'videos/particle-conf.tracks'.

3. ```python3 analyzer.py 'videos/particles-conf.tracks' --samples [1,34,67] --view_points```

    This, get the filtered tracks file of previous example and draw the points of the tracks with ids 1, 34, 67.

4. ```python3 analyzer.py 'videos/particles.tracks' --n_samples 10 --epsilon 8 --view_chd```

    This, filter 10 tracks and run the change of direction algorithm with epsilon 8 and view one by one the results.

5. ```python3 analyzer.py 'videos/particles.tracks' --n_samples 10 --epsilon 8 --save_data```

    This example do the same of the previous example but save the json file with data of the tracks.
