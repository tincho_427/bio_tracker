# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import numpy as np
np.seterr(all='raise')

import math


def _rint(x):
    return int(np.rint(x))


def degree(direction):
    """
    Calculate degrees of between consecutive vectors, and return result in radians
    """
    dir2 = direction[1:]
    dir1 = direction[:-1]
    norm1 = (dir1**2).sum(axis=1)
    norm2 = (dir2**2).sum(axis=1)
    degrees = np.arccos((dir1 * dir2).sum(axis=1) / (np.sqrt(norm1 * norm2) + 2**-23))
    # degrees = [0 if math.isnan(deg) else deg for deg in degrees]
    return np.array(degrees)


def mad(data, axis=None):
    return np.mean(np.absolute(data - np.median(data, axis)), axis)


def move_at_least_nbodies(track, particle_size, n_bodies):
    """
    Return true if particle move at least n_bodies in one dimension
    """
    min_x, min_y = np.min(track['pt'], axis=0)
    max_x, max_y = np.max(track['pt'], axis=0)
    x_diff = (max_x - min_x)
    y_diff = (max_y - min_y)
    if max(x_diff, y_diff) > particle_size * n_bodies:
        return True
    else:
        False


def get_times_from_tumbles(track, tumble_pts):
    """
    Get the times of the tumble points
    """
    times_t = []
    tumble_pts = [tumble_pt.tolist() for tumble_pt in tumble_pts]
    for i, pt in enumerate(track['pt']):
        if pt in tumble_pts:
            times_t.append(track['t'][i])
    return times_t


def get_vrel(track):
    """
    Index to determinate linearity based on instant velocities
    """
    pts = track['pt']
    n = len(pts)
    # mean velocity in a straight between first and last point of the track
    L = np.sqrt((pts[-1][0] - pts[0][0])**2 + (pts[-1][1] - pts[0][1])**2)
    T = len(track['t'])
    # next we will calculate the average of instantaneous speeds
    s_li = get_distance_traveled(track)
    mean_inst_vel = s_li / (n - 1)
    I1 = (L / T) / mean_inst_vel
    return I1


def get_mean_velocity(track):
    """
    Index to determinate linearity based on eigenvalues
    """
    distance = get_distance_traveled(track)
    time = len(track['t'])
    return (distance / time)


def get_distance_traveled(track):
    """
    Distance traveled between first and last point of the track, i.e. sum of
    distances between each pair of consecutive points
    """
    total_distance = 0
    point_a = track['pt'][0]
    for i in range(1, len(track['pt'])):
        point_b = track['pt'][i]
        dist2 = (point_b[0] - point_a[0])**2 + (point_b[1] - point_a[1])**2
        total_distance += np.sqrt(dist2)
        point_a = point_b
    return total_distance


def get_L(track):
    """
    Distance in a straight line, between first and last point of a track
    """
    pts = track['pt']
    # distance between first and last point
    L = np.sqrt((pts[-1][0] - pts[0][0])**2 + (pts[-1][1] - pts[0][1])**2)
    return L


def get_mean_degree_b3pts(track):
    """
    Calculate media degree (in radians) between vectors formed by three
    consecutive points
    """
    points = track['pt']
    if len(points) > 2:
        directions = np.diff(points, axis=0)
        theta = degree(directions)
        mean_theta = np.mean(theta)
    else:
        print('WARNING: can\'t compute theta for track lenghts lower than 2.'
              'If possible, set the --min_lenght=3 or to a higher value')
        mean_theta = 0.

    return mean_theta
