import helpers
from nose.tools import assert_equals, with_setup
import numpy as np
import os

class TestHELPERS():
    """ Test helpers functions

    Simulte simple situations to test the most complex functions in the helpers module.
    Simple functions are not tested.
    """

    @classmethod
    def setup_class(cls):
        # create a track with 10 point and a tumble in the point number 6 of 45°
        x = np.linspace(2.0, 11.0, 10)
        y = np.concatenate((np.ones(6), np.linspace(2.0, 5.0, 4)))
        pts = np.vstack((x, y)).T
        times = np.linspace(0, 9, 10)
        cls.track = {'pt': pts.tolist(), 't': times}
        cls.tumbles_idxs = np.array([5])

    def test_degree(self):
        # test the function degree with two vectors that form an angle of 45°
        v1 = [1,0]
        v2 = [1,1]
        dir = np.array([v1, v2])
        rad_degres = helpers.degree(dir)
        degrees = (rad_degres * 180) / np.pi
        assert_equals(int(degrees[0]), 45)

    def test_get_times_from_tumbles(self):
        # use a simulated track with a tumble in 6th point to get the time of the
        # tumble in the track.
        tumble_pts = np.array([self.track['pt'][i] for i in self.tumbles_idxs])
        times_tumbles = helpers.get_times_from_tumbles(self.track, tumble_pts)
        assert_equals(len(times_tumbles), len(self.tumbles_idxs))
        assert_equals(times_tumbles[0], self.tumbles_idxs[0])

    def test_get_distance_traveled(self):
        # check that the distance traveled is ok
        dist_traveled = helpers.get_distance_traveled(self.track)
        real_dist = 5 + 4 * np.sqrt(2)
        assert_equals(round(dist_traveled, 5), round(real_dist, 5))

    def test_get_mean_degree_b3pts(self):
        # check if the get_mean_degree_b3pts its ok using the test track, wich
        # always move in straight line, except in one point wich tumble with 45° degrees
        mean_theta = helpers.get_mean_degree_b3pts(self.track)
        # calculate the mean real of the track (first pass 45° to radians)
        real_mean_theta = ((45 / 180) * np.pi) / (len(self.track['pt']) - 2)
        assert_equals(round(mean_theta, 5), round(real_mean_theta, 5))
