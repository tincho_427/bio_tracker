	#!/usr/bin/make

# Viruses
#     Particle shape: spherical
#     Dynamics: same direction, switch between brownian and linear
#     PSF: Airy disk profile
#     Densities {low, mid, high}: {100, 500, 1000}
#     Dimensionality: 3D+time
#     z-step: 300nm
#     Image size: 512x512 pixels
#     Image depth: 10 slices
#     Image length: 100 frames

# Vesicles
#     Particle shape: spherical
#     Dynamics: brownian, any direction
#     PSF: Airy disk profile
#     Densities {low, mid, high}: {100, 500, 1000}
#     Dimensionality: 2D+time
#     Image size: 512x512 pixels
#     Image length: 100 frames

# Receptors
#     Particle shape: spherical
#     Dynamics: tethered motion, switching, any direction
#     PSF: Airy disk profile
#     Densities {low, mid, high}: {100, 500, 1000}
#     Dimensionality: 2D+time
#     Image size: 512x512 pixels
#     Image length: 100 frames

# Microtubules
#     Particle shape: slightly elongated (tips)
#     Dynamics: nearly constant velocity model
#     PSF: Gaussian intensity profile
#     Densities {low, mid, high}: {60, 400, 700}
#     Dimensionality: 2D+time
#     Image size: 512x512 pixels
#     Image length: 100 frames

.PHONY: eval

eval:
	mkdir -p eval
	for scenario in RECEPTOR; do \
	  for density in high; do \
	    for snr in 4; do \
			  rm -f eval/$${scenario}\ snr\ $${snr}\ density\ $${density}.log  \
				  SCENARIO=$${scenario} SNR=$${snr} DENSITY=$${density}; \
	      make -s eval/$${scenario}\ snr\ $${snr}\ density\ $${density}.log \
	        SCENARIO=$${scenario} SNR=$${snr} DENSITY=$${density}; \
	      cat eval/$${scenario}\ snr\ $${snr}\ density\ $${density}.log \
	        | grep -e alpha -e beta -e Jaccard; echo ""; \
	    done; \
	  done; \
	done

# BASEPATH = $(HOME)/Datasets/ParticleTrackingChallenge/
BASEPATH = $(HOME)/Escritorio/particle_challenge/
BASENAME = $(SCENARIO)\ snr\ $(SNR)\ density\ $(DENSITY)
# IMAGES = $(BASEPATH)/$(SCENARIO)/$(BASENAME)/$(BASENAME)\ t%03d\ z0.tif
# REFERENCE = $(BASEPATH)/$(SCENARIO)/$(BASENAME)/$(BASENAME).xml
# IMAGES = $(BASEPATH)/challenge/$(SCENARIO)/$(BASENAME)/$(BASENAME)\ t%03d\ z0.tif
IMAGES = $(BASEPATH)/$(SCENARIO)/$(BASENAME)/$(BASENAME)\ t%03d\ z0.tif
# REFERENCE = $(BASEPATH)/challenge/$(SCENARIO)/$(BASENAME)/$(BASENAME).xml
REFERENCE = $(BASEPATH)/$(SCENARIO)/$(BASENAME)/$(BASENAME).xml

DETECTOR_PARAM = --invert=1 --sigma=1.3 --thr=0.4 --avg=1
LINKER_PARAM = --min_dist=6 --mean_dist_thr=1.0 --dist_ratio_thr=0.8 --max_tgap=1 --kalman --n_dry_run 25 --kf_thr 5 --kf_long 1

DETECTOR_PARAM2 = --invert=1 --sigma=1.3 --thr=0.4 --avg=1
LINKER_PARAM2 = --min_dist=6 --mean_dist_thr=1.0 --dist_ratio_thr=0.8 --max_tgap=1  --n_dry_run 25 --kf_thr 5 --kf_long 100

eval/$(BASENAME).log:
	python detector.py $(IMAGES) $(DETECTOR_PARAM) --out=eval/$(BASENAME).detections
	python linker.py eval/$(BASENAME).detections --xml $(LINKER_PARAM) --out=eval/$(BASENAME).tracks
	java -jar $(BASEPATH)/trackingPerformanceEvaluation/trackingPerformanceEvaluation.jar -r $(REFERENCE) -c eval/$(BASENAME).xml > "$@"


# SCENARIO = RECEPTOR
# DENSITY = mid
# SNR = 2
SCENARIO = VESICLE
DENSITY = low
SNR = 7
eval1:
	python detector.py $(IMAGES) --out=eval/$(BASENAME).detections --view \
	  --avg=1 --invert=1 --sigma=1.5 --thr=0.5
	python linker.py eval/$(BASENAME).detections--out=/tmp/$(BASENAME).tracks --xml \
	  --min_dist=5 --mean_dist_thr=5.0 --dist_ratio_thr=0.8 --max_tgap=2
	java -jar $(BASEPATH)/trackingPerformanceEvaluation.jar -r $(REFERENCE) -c /tmp/$(BASENAME).xml














.PHONY: eval2

eval2:
	mkdir -p eval2
	for scenario in RECEPTOR; do \
	  for density in high; do \
	    for snr in 4; do \
			  rm -f eval/$${scenario}\ snr\ $${snr}\ density\ $${density}_2.log  \
				  SCENARIO=$${scenario} SNR=$${snr} DENSITY=$${density}; \
	      make -s eval/$${scenario}\ snr\ $${snr}\ density\ $${density}_2.log \
	        SCENARIO=$${scenario} SNR=$${snr} DENSITY=$${density}; \
	      cat eval/$${scenario}\ snr\ $${snr}\ density\ $${density}_2.log \
	        | grep -e alpha -e beta -e Jaccard; echo ""; \
	    done; \
	  done; \
	done

eval/$(BASENAME)_2.log:
	python detector.py $(IMAGES) $(DETECTOR_PARAM2) --out=eval/$(BASENAME).detections
	python linker.py eval/$(BASENAME).detections --xml $(LINKER_PARAM2) --out=eval/$(BASENAME).tracks
	java -jar $(BASEPATH)/trackingPerformanceEvaluation/trackingPerformanceEvaluation.jar -r $(REFERENCE) -c eval/$(BASENAME).xml > "$@"
