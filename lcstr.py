# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

import numpy as np
from numpy.ctypeslib import ndpointer
from ctypes import cdll, c_void_p, c_bool, c_float, c_int, byref
from os.path import abspath, join, pardir, dirname, realpath

libutils = cdll.LoadLibrary(abspath(join(dirname(realpath(__file__)), 'build/libutils.so')))

_lcstr = libutils.lcstr
_lcstr.argtypes = [
    ndpointer(dtype=np.float32, ndim=1, flags='C,A'),     # s1
    c_int,                                                # n1
    ndpointer(dtype=np.float32, ndim=1, flags='C,A'),     # s2
    c_int,                                                # n2
    c_float,                                              # eps
    c_void_p,                                             # i1
    c_void_p                                              # i2
]
_lcstr.restype = c_int

def lcstr(s1, s2, eps=1e-6):
    s1 = np.require(s1, dtype=np.float32, requirements=('C', 'A'))
    n1 = c_int(len(s1))
    s2 = np.require(s2, dtype=np.float32, requirements=('C', 'A'))
    n2 = c_int(len(s2))
    eps = c_float(eps)
    i1 = c_int()
    i2 = c_int()
    len_lcstr = _lcstr(s1, n1, s2, n2, eps, byref(i1), byref(i2))
    return len_lcstr, i1.value, i2.value

# # https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring#Python
# def longest_common_substring(s1, s2):
#     m = [[0] * (1 + len(s2)) for _ in xrange(1 + len(s1))]
#     longest, x_longest, y_longest = 0, 0, 0
#     for x in xrange(1, 1 + len(s1)):
#         for y in xrange(1, 1 + len(s2)):
#             if s1[x - 1] == s2[y - 1]:
#                 m[x][y] = m[x - 1][y - 1] + 1
#                 if m[x][y] > longest:
#                     longest = m[x][y]
#                     x_longest = x
#                     y_longest = y
#             else:
#                 m[x][y] = 0
#     return s1[x_longest - longest: x_longest], x_longest - longest, y_longest - longest
