from chd_detector import *
from nose.tools import assert_equals, assert_not_equal, with_setup
import numpy as np

class TestCHDDETECTOR():
    """ Test the change directions detector functions

    Simulte a simple track with ten points and one tumble of 45° and test the
    class of CHDDetector.
    """

    @classmethod
    def setup_class(cls):
        # create a CHDDetector object
        cls.param = {'min_degree': 0,
                     'max_degree': 180,
                     'epsilon': 1.0,
                     'min_dist': 5}
        cls.chd_detector = CHDDetector(**cls.param)

        # create a track with 10 point and a tumble in the point number 6 of 45°
        x = np.linspace(2.0, 11.0, 10)
        y = np.concatenate((np.ones(6), np.linspace(2.0, 5.0, 4)))
        pts = np.vstack((x, y)).T
        times = np.linspace(0, 9, 10)
        cls.track = {'pt': pts, 't': times}
        cls.tumbles_idxs = np.array([5])

    def test_detector_create(self):
        # test if it could instance a CHange Direction Detector object
        assert_not_equal(self.chd_detector, None)

    def test_get_tumbles(self):
        # check if get correct tumbles and degrees for a list with only the
        # test track (it have only one tumble of 45°)
        tumbles, degrees = self.chd_detector.get_tumbles([self.track])
        assert_equals(len(tumbles), 1) # check for each track
        assert_equals(len(tumbles[0]), len(self.tumbles_idxs)) # check numbers of tumbles for test track
        np.testing.assert_equal(tumbles[0][0], self.track['pt'][self.tumbles_idxs[0]]) # check tumbles for test track
        assert_equals(len(degrees), 1) # check for each track
        assert_equals(len(degrees[0]), len(self.tumbles_idxs)) # check numbers of tumbles degrees for test track
        assert_equals(round(degrees[0][0], 5), 45) # check tumbles for test track
